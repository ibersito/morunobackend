<?php
namespace app\components;
class PFunciones{
    public static function pageTotal($provider, $fieldName)
    {
        $total = 0;
        foreach ($provider as $item) {
            $total += $item[$fieldName];
        }
        return $total;
    }

    public static function edad($provider)
    {
        foreach ($provider as $item) {
            $item->edad = date("Y") - date("Y", strtotime($item->fecha_nacimiento));
            $item->save();
        }
    }
    public static function ficha($provider)
    {
        $numero = 1;
        foreach($provider as $item){
            $item->num_ficha = $numero++;
            $item->save();
        }
    }
}