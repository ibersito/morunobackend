<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emergencia".
 *
 * @property int $id
 * @property int $paciente_id
 * @property string $fecha
 * @property string $hora
 * @property string $hora_ingreso
 * @property string $tipo
 * @property int $tratamiento_id
 * @property string $observacion
 *
 * @property TipoTratamiento $tratamiento
 * @property Paciente $paciente
 */
class Emergencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emergencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora', 'hora'], 'required'],
            [['paciente_id', 'tratamiento_id'], 'integer'],
            [['fecha', 'hora', 'hora_ingreso'], 'safe'],
            [['tipo','observacion'], 'string'],
            [['tratamiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoTratamiento::className(), 'targetAttribute' => ['tratamiento_id' => 'id']],
            [['paciente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['paciente_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paciente_id' => 'Paciente',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'hora_ingreso' => 'Hora Ingreso',
            'tipo' => 'Tipo',
            'tratamiento_id' => 'Tratamiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTratamiento()
    {
        return $this->hasOne(TipoTratamiento::className(), ['id' => 'tratamiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'paciente_id']);
    }
}