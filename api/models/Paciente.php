<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paciente".
 *
 * @property int $id
 * @property string $nombres
 * @property string $apellidos
 * @property string $fecha_nacimiento
 * @property int $edad
 * @property string $telefono
 * @property string $celular
 *
 * @property Kardex[] $kardexes
 * @property Reserva[] $reservas
 */
class Paciente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombres', 'apellidos', 'fecha_nacimiento', 'edad', 'telefono', 'celular'], 'required'],
            [['fecha_nacimiento'], 'safe'],
            [['edad'], 'integer'],
            [['nombres', 'apellidos'], 'string', 'max' => 255],
            [['telefono', 'celular'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'edad' => 'Edad',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmergencias()
    {
        return $this->hasMany(Emergencia::className(), ['paciente_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKardexes()
    {
        return $this->hasMany(Kardex::className(), ['paciente_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasMany(Receta::className(), ['paciente_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['paciente_id' => 'id']);
    }

    public function getNombrecompleto()
    {
        return $this->nombres . ' ' . $this->apellidos;
    }
}