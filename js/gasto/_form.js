var div = document.getElementById("oculto");
// div.style.display = "none";
div.style.visibility = "hidden";
$(document).ready(function () {
    $('#gasto-tipo_gasto').change(function () {
        mostrar();
    });
});
function mostrar() {

    // $('#gasto-tratamiento_id').prop('selected', true);
    $("#gasto-tratamiento_id").each( function() {
        $(this).val( $(this).find("option[selected]").val() );
    });


    $('#gasto-cantidad').val("");
    var tipo_gasto = $("#gasto-tipo_gasto").select2("val");
    if (tipo_gasto == 'Tratamiento') {
        div.style.visibility = "visible";
    }else if(tipo_gasto == 'General'){
        div.style.visibility = "hidden";
    }
}