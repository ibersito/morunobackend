<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use app\widgets\Alert;
use yii\widgets\Pjax;

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
?>
   <?=$this->render('_servicios_site', [
      'zonas'=>$zonas,
      'id'=>$id
   ])?>
   <!--Location Map here-->
   <!-- <div id="map-container"></div>    -->
   <?php 
      $coord = new LatLng([
         'lat' => -17.397405, 
         'lng' => -66.162607
      ]);
      $map = new Map([
            'center' => $coord,
            'zoom' =>12,
            // 'width' => 100,
            // 'height' => '600px',
      ]);
      
      foreach ($espaciosPublicitarios as $key => $espacio) {
         $coordPoint = new LatLng([
            'lat' => $espacio->latitude, 
            'lng' => $espacio->longitude
         ]);
         // Lets add a marker now
         $marker = new Marker([
               'position' => $coordPoint,
               // 'icon'=>'/images/map_marker_icon.png',
               'icon'=>$espacio->icono?Url::to('@web/uploads'.\app\models\EspacioPublicitario::PATH.$espacio->icono):'/images/map_marker_icon.png',
               'title' => 'Codigo',
         ]);
         // Provide a shared InfoWindow to the marker
         $marker->attachInfoWindow(
            new InfoWindow([
               'content' => 'Codigo: T596d <br> 
                  Tamaño:50m x 40m <br> 
                  Direccion: calle aniceto padilla n° 299 <br>
                  Caras: 1 <br>
                  Iluminacion: 2 reflectores <br>'
            ])
         );
   
         $map->addOverlay($marker);  
      }

      $map->width = '100%';
      $map->height = '450';
      // $map->backgroundColor = 'red';

      // Display the map -finally :)
      echo $map->display();
   ?>
   <!-- </section> -->
   <!--Contact US Ends--> 
