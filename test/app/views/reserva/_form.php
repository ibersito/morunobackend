<?php

use yii\helpers\Html;
use app\models\Paciente;
use app\models\TipoTratamiento;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reserva-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <?= $form->field($model, 'paciente_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Paciente::find()->all(), 'id', 'nombrecompleto'),
                            'options' => ['placeholder' => 'Seleccione un paciente...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                    ?>                
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" style="margin-top: 20px;">
                <div class="form-group">
                    <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Nuevo paciente', ['/paciente/create', 'tipo'=>$tipo,'id'=>$model->id], ['class'=>'btn btn-success waves-effect waves-classic'])?>
                </div>
            </div>
        </div>


        <?= $form->field($model, 'fecha')->widget(
            DatePicker::className(),
                [
                    'name' => 'check_issue_date',
                    'value' => date('y-m-d', strtotime('+2 days')),
                    'options' => ['placeholder' => 'Seleccione la fecha ...'],
                    'pluginOptions' => [
                        // 'startDate' => date('y-m-d'),
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                        // 'todayHighlight' => true
                    ]
                ]
            );
        ?>

        <?= $form->field($model, 'hora')->widget(
            TimePicker::className(),
                [
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 15,
                        // 'secondStep' => 5,
                    ],
                ]
            );
        ?>
        <?php if(!$model->isNewRecord){ ?>
            <?= $form->field($model, 'hora_ingreso')->widget(
                TimePicker::className(),
                    [
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            // 'secondStep' => 5,
                        ]
                    ]
                );
            ?>

            <?= $form->field($model, 'hora_llegada')->widget(
                TimePicker::className(),
                    [
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            // 'secondStep' => 5,
                        ]
                    ]
                );
            ?>
        <?php } ?>

        <?= $form->field($model, 'tipo')->widget(Select2::classname(), [
                'data' => [
                    'Consulta' => 'Consulta',
                    'Reconsulta' => 'Reconsulta',
                    'Tratamiento' => 'Tratamiento',
                ],
                'options' => ['placeholder' => 'Seleccione una opcion ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        ?>

        <?= $form->field($model, 'tratamiento_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TipoTratamiento::find()->all(), 'id', 'nombre'),
                'options' => ['placeholder' => 'Seleccione un tratamiento...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        ?>

        <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

        <div class="form-group text-right">
            <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['index'], [
                'class' => 'btn btn-default',
                'title' => 'Cerrar',
            ]) ?>
            <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>