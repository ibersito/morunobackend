<?php

use app\models\Paciente;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\TipoTratamiento;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reporte de tratamientos';
$this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="plan-index">
    <div class="card">
        <div class="card-content">
            <div class="row" style="background-color:#f5f5f5; border-radius:15px;">
                <?php $form = ActiveForm::begin([
                    // 'type' => ActiveForm::TYPE_HORIZONTAL,
                    'options' => [
                        'class' => 'disable-submit-buttons',
                    ]
                ]); ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <?= $form->field($model, 'fechaInicio')->hint('Año - Mes - Dia')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Seleccione la fecha', 'autocomplete' => 'off'],
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <?= $form->field($model, 'fechaFin')->hint('Año - Mes - Dia')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Seleccione la fecha', 'autocomplete' => 'off'],
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <?= $form->field($model, 'tratamiento_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(TipoTratamiento::find()->all(), 'id', 'nombre'),
                                    'options' => ['placeholder' => 'Seleccione un tratamiento...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <?= $form->field($model, 'paciente_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Paciente::find()->all(), 'id', 'nombrecompleto'),
                                    'options' => ['placeholder' => 'Seleccione un paciente...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                                ?>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <?= $form->field($model, 'tipo_paciente')->widget(Select2::classname(), [
                                        'data' => [
                                            'TRANSPORTISTAS' => 'TRANSPORTISTAS',
                                            'ELFEC' => 'ELFEC',
                                        ],
                                        'options' => ['placeholder' => 'Seleccione una opcion ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                ?>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                <div class="form-group" style="padding-top:15px;">
                                    <?= Html::submitButton('<i class="material-icons">search</i> Buscar', ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                <div class="form-group" style="padding-top: 15px;padding-left:10px;">
                                    <?= Html::a('<i class="material-icons">autorenew</i> Limpiar', ['reporte/tratamientos'], ['class' => 'btn btn-warning btn-round']) ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                <div class="form-group" style="padding-top:15px;padding-left:15px">
                                    <?= Html::a('<i class="material-icons">print</i> Imprimir', ['reportetratamientospdf', 'fechaInicio' => $fechaInicio, 'fechaFin' => $fechaFin, 'tratamiento' => $tratamiento, 'paciente' =>$paciente, 'telefono' => $telefono, 'tipo_paciente' => $tipo_paciente], ['class' => 'btn btn-info btn-round', 'target' => '_blank']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
                
            </div>

            <?= GridView::widget([
                'dataProvider' => $tratamientos,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Paciente',
                        'attribute' => 'paciente.nombrecompleto'
                    ],
                    [
                        'label' => 'Tratamiento',
                        'attribute' => 'tratamiento.nombre'
                    ],
                    [
                        'label' => 'Monto Total',
                        'attribute' => 'pago.monto_total'
                    ],
                    [
                        'label' => 'Cantidad jeringas',
                        'attribute' => 'pago.cantidad_jeringas'
                    ],
                    [
                        'label' => 'Nume Doc',
                        'attribute' => 'num_doc'
                    ],
                ],
                'bordered' => true,
                'striped' => false,
                'hover' => true,
                'condensed' => false,
                'responsive' => true,
                'responsiveWrap' => false,
                'resizableColumns' => false,
            ]); ?>

            <!-- <div class="form-group text-right">
                <?= Html::a('<i class="material-icons">clear</i> Cerrar', ['index'], ['class' => 'btn btn-round btn-default']) ?>
            </div> -->
        </div>
    </div>
</div>