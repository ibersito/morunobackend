<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de saldos</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>Tratamiento</th>
                        <th>Saldo</th>
                        <th>Cantidad jeringas</th>
                        <th>Nume Doc</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($saldos->models as $saldo){ ?>
                        <tr>
                            <td><?= $saldo->paciente->nombrecompleto ?></td>
                            <td><?= $saldo->tratamiento->nombre ?></td>
                            <td><?= $saldo['saldo'] ?></td>
                            <td><?= $saldo['cantidad'] ?></td>
                            <td><?= $saldo['num_doc'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>