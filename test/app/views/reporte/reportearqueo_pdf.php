<?php

use app\components\PFunciones;
?>
<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de Arqueo</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div id="w1" class="grid-view is-bs3 hide-resize" data-krajee-grid="kvGridInit_a5ee775f" data-krajee-ps="ps_w1_container">
                            <div class="table-responsive kv-grid-container">
                                <table class="kv-grid-table table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Ingreso</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td><b>TOTALES</b></td>
                                            <td><b><?= PFunciones::pageTotal($arqueoingreso, 'monto_pago') ?></b></td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($arqueoingreso); $i++) { ?>
                                            <tr>
                                                <td><?= $arqueoingreso[$i]['tipo'] ?></td>
                                                <td><?= $arqueoingreso[$i]['monto_pago'] ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div id="w1" class="grid-view is-bs3 hide-resize" data-krajee-grid="kvGridInit_a5ee775f" data-krajee-ps="ps_w1_container">
                            <div class="table-responsive kv-grid-container">
                                <table class="kv-grid-table table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tipo Gasto</th>
                                            <th>Egresos</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td><b>TOTALES</b></td>
                                            <td><b><?= PFunciones::pageTotal($arqueoegreso, 'monto') ?></b></td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($arqueoegreso); $i++) { ?>
                                            <tr>
                                                <td><?= $arqueoegreso[$i]['tipo_gasto'] ?></td>
                                                <td><?= $arqueoegreso[$i]['monto'] ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-md-6 col-md-offset-3">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Total Ingresos</th>
                                    <th>Total Egresos</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td><b>TOTALES</b></td>
                                    <td><b><?= PFunciones::pageTotal($arqueoingreso, 'pagado') - PFunciones::pageTotal($arqueoegreso, 'monto') ?></b></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td><?= PFunciones::pageTotal($arqueoingreso, 'monto_pago') ?></td>
                                    <td><?= PFunciones::pageTotal($arqueoegreso, 'monto') ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>