<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de tratamientos</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>Tratamiento</th>
                        <th>Monto Total</th>
                        <th>Cantidad jeringas</th>
                        <th>Nume Doc</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($tratamientos->models as $tratamiento){ ?>
                        <tr>
                            <td><?= $tratamiento->paciente->nombrecompleto ?></td>
                            <td><?= $tratamiento->tratamiento->nombre ?></td>
                            <td><?= $tratamiento['monto'] ?></td>
                            <td><?= $tratamiento['cantidad'] ?></td>
                            <td><?= $tratamiento['num_doc'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>