<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\TipoTratamiento;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */

$this->title = 'Paciente ' . $paciente->nombrecompleto;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reserva-view">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
            <div class="card">
                <div class="card-content">
                    <h3 class="text-center reporte">
                        Kardex Paciente
                        <!-- <a class="btn btn-info" href="<?= Url::to(['paciente/kardexpdf', 'id' => $paciente->id]) ?>" target="_blank">
                            <i class="material-icons">print</i> Imprimir
                        </a> -->
                    </h3>
                    <hr>
                    <table class="table">
                        <tbody style="background-color:#A9D0F5">
                            <tr>
                                <td>
                                    <strong> Nombre: </strong> <?= $paciente->nombrecompleto ?>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong> Edad: </strong> <?= $paciente->edad ?> Años
                                </td>
                                <td>
                                    <strong> Fecha Nacimiento: </strong> <?= $paciente->fecha_nacimiento ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong> Telefono: </strong> <?= $paciente->telefono ?> - <?= $paciente->celular ?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="row">
                        <?php $form = ActiveForm::begin([
                            'options'=>[
                                'class' => 'disable-submit-buttons',
                            ] 
                        ]); ?>
                        <div class="table-responsive">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'fecha')->widget(
                                                    DatePicker::className(),
                                                    [
                                                        'name' => 'check_issue_date',
                                                        'value' => date('y-m-d', strtotime('+2 days')),
                                                        'options' => ['placeholder' => 'Seleccione la fecha ...'],
                                                        'pluginOptions' => [
                                                            'format' => 'yyyy-mm-dd',
                                                            'autoclose' => true,
                                                            'todayHighlight' => true
                                                        ]
                                                    ]
                                                );
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'tipo')->widget(Select2::classname(), [
                                                    'data' => [
                                                        'Consulta' => "Consulta",
                                                        'Reconsulta' => "Reconsulta",
                                                        'Tratamiento' => "Tratamiento",
                                                    ],
                                                    'options' => ['placeholder' => 'Seleccione una opcion ...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'tratamiento_id')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(TipoTratamiento::find()->where(['>', 'cantidad', '0'])->all(), 'id', 'nombre'),
                                                    'options' => ['placeholder' => 'Seleccione un tratamiento...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'detalle')->textarea(['maxlength' => true, 'rows' => '6']) ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'num_pagina')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'num_doc')->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" style="padding-top: 25px;">
                                            <div class="form-group">
                                                <button name="agregar" type="submit" class="btn btn-success" id="bt_agregar">
                                                    <i class="material-icons">save</i>Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Tipo</th>
                                <th>Tipo tratamiento</th>
                                <th>Detalle</th>
                                <th>N° Doc</th>
                                <th>Num Pagina</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($kardexes as $kardex) { ?>
                                <tr <?= $kardex->getEstados($kardex->estado) ?>>
                                    <td width="7%"><?= $kardex->fecha ?></td>
                                    <td width="7%"><?= $kardex->tipo ?></td>
                                    <td width="8%"><?= $kardex->tratamiento ? $kardex->tratamiento->nombre : '' ?></td>
                                    <td width="30%"><?= $kardex->detalle ?></td>
                                    <td><?= $kardex->num_doc ?></td>
                                    <td><?= $kardex->num_pagina ?></td>
                                    <td><?= $kardex->estado ?></td>
                                    <td class="text-center">
                                        <?php if ($kardex->estado == 'Pendiente') { ?>
                                            <?php if (Yii::$app->user->can('Administrador') || Yii::$app->user->can('Secretaria')) { ?>
                                                <a class="btn btn-success btn-round btn-just-icon" href="<?= Url::to(['paciente/estado', 'id' => $kardex->id]) ?>" title="Cambiar estado" aria-label="Cambiar estado" data-confirm="¿ Esta seguro de cambiar de estado este elemento ?" data-method="post">
                                                    <i class="material-icons">remove_circle_outline</i>
                                                </a>
                                            <?php } ?>
                                          
                                        <?php } ?>

                                        <a href="<?= Url::to(['paciente/kardexupdate', 'id' => $kardex->id]) ?>" class="btn btn-warning btn-round btn-just-icon" title="Editar kardex">
                                                <i class="material-icons">edit</i>
                                            </a>

                                        <a class="btn btn-info btn-round btn-just-icon" href="<?= Url::to(['paciente/kardexpdf', 'id' => $kardex->id, 'paciente_id' => $kardex->paciente_id]) ?>" target="_blank">
                                            <i class="material-icons">print</i>
                                        </a>
                                        <a class="btn btn-danger btn-round btn-just-icon" href="<?= Url::to(['paciente/pagos', 'id' => $kardex->id]) ?>" title="Kardex pagos">
                                            <i class="material-icons">attach_money</i>
                                        </a>
                                        <?php //if($kardex->tipo == 'Consulta'){ ?>
                                            <a class="btn btn-warning btn-round btn-just-icon" href="<?= Url::to(['paciente/pagoconsulta', 'id' => $kardex->id]) ?>" title="Kardex pago consulta">
                                                <i class="material-icons">attach_money</i>
                                            </a>
                                        <?php //} ?>
                                        <a href="<?= Url::to(['paciente/receta', 'id' => $kardex->id]) ?>" class="btn btn-primary btn-round btn-just-icon" title="Receta medica">
                                            <i class="material-icons">assignment</i>
                                        </a>

                                        <!-- <a class="btn btn-danger btn-just-icon btn-round" 
                                               href="<?= Url::to(['paciente/kardexdelete', 'id' => $kardex->id]) ?>" 
                                               title="Eliminar"
                                               aria-label="Eliminar"
                                               data-confirm="¿ Esta seguro de eliminar este elemento ?"
                                               data-method="post"
                                            >
                                                <i class="material-icons">delete</i>
                                            </a> -->
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="form-group text-right">
                        <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['index'], ['class' => 'btn btn-default', 'title' => 'Cerrar',]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>