<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="reporte" class="size-middle-carta visible-print-block text-uppercase">
    <!-- <div class="size-middle-carta text-uppercase"> -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
            <div class="card">
                <div class="card-content">
                    <span>
                        <div class="medico">
                            Dr. Henry Moruno Cruz <br>
                            <u>MÉDICO REUMATÓLOGO</u>
                        </div>
                    </span>
                    <h3 class="text-center">
                        HISTORIA CLINICA
                    </h3>
                    <span>
                        <div class="num-pagina">
                            <p><?= $kardex->num_pagina ?></p>
                        </div>
                    </span>
                    <div class="noborder">
                        <table class="table table-striped table-condensed table-hover" style="font-size: 15px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <strong> Nombre: </strong> <?= $paciente->nombrecompleto ?>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong> Edad: </strong> <?= $paciente->edad ?> Años
                                    </td>
                                    <td>
                                        <strong> Fecha: </strong> <?= date('d-m-Y') ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong> Telefono: </strong> <?= $paciente->telefono ?> - <?= $paciente->celular ?>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="separacion">
                        ---------------------------------------------------------
                    </div>
                    <!-- <table class="table table-striped table-condensed table-hover" style="font-size: 13px;">
                        <thead>
                            <tr>
                                <th scope="col">Fecha</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Tipo tratamiento</th>
                                <th scope="col">Detalle</th>
                                <th scope="col">N° Doc</th>
                                <th scope="col">Monto Bs</th>
                                <th scope="col">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($kardexes as $kardex) { ?>
                                <tr>
                                    <td><?= $kardex->fecha ?></td>
                                    <td><?= $kardex->tipo ?></td>
                                    <td><?= $kardex->tratamiento ? $kardex->tratamiento->nombre : '' ?></td>
                                    <td width="30%"><?= $kardex->detalle ?></td>
                                    <td><?= $kardex->num_doc ?></td>
                                    <td><?= $kardex->pago ? $kardex->pago->monto_total : '' ?></td>
                                    <td><?= $kardex->pago ? $kardex->pago->cantidad_jeringas : '' ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table> -->
                </div>
            </div>
        </div>
    </div>
</div>