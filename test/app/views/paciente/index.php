<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paciente-index">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left">
                            <h4>
                                <?= Html::encode($this->title) ?>
                            </h4>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right pull-right">
                            <?php if (Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) { ?>
                                <?= Html::a("<i class='material-icons'>add</i> Nuevo " . 'Paciente', ['create'], ['class' => 'btn btn-success']) ?>
                            <?php } ?>
                        </div>
                    </div>

                    <?php // echo $this->render('_search', ['model' => $searchModel]); 
                    ?>


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nombres',
                            'apellidos',
                            'fecha_nacimiento',
                            // [
                            //     'label' => 'Fecha Nacimiento',
                            //     'attribute' => 'fecha_nacimiento',
                            //     'filterType' => '\kartik\date\DatePicker',
                            //     'format' => ['date', 'php:d-m-Y'],
                            //     'width' => '100px',
                            //     'filterWidgetOptions' => [
                            //         'language' => 'es',
                            //         'type' => DatePicker::TYPE_INPUT,
                            //         'removeButton' => false,
                            //         'pluginOptions' => [
                            //             'autoclose' => true,
                            //             'format' => 'yyyy-mm-dd'
                            //         ]
                            //     ],
                            // ],
                            'edad',
                            'telefono',
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to([$action, 'id' => $key]);
                                },
                                'width' => '200px',
                                'template' => '{kardex} {view} {update} {delete}',
                                'buttons' => [
                                    'view' => function ($url, $model, $key) {
                                        if (Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) {
                                            return Html::a('<i class="material-icons">visibility</i>', $url, [
                                                'class' => 'btn btn-info btn-round btn-just-icon',
                                                'title' => 'Ver',
                                                'aria-label' => 'Ver',
                                                'data-pjax' => 0
                                            ]);
                                        }
                                    },
                                    'update' => function ($url, $model, $key) {
                                        if (Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) {
                                            return Html::a('<i class="material-icons">edit</i>', $url, [
                                                'class' => 'btn btn-warning btn-round btn-just-icon',
                                                'title' => 'Modificar',
                                                'aria-label' => 'Modificar',
                                                'data-pjax' => 0
                                            ]);
                                        }
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        if (Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) {
                                            return Html::a('<i class="material-icons">delete</i>', $url, [
                                                'class' => 'btn btn-danger btn-round btn-just-icon',
                                                'title' => 'Eliminar',
                                                'aria-label' => 'Eliminar',
                                                'data-confirm' => '¿ Esta seguro de eliminar este elemento ?',
                                                'data-method' => 'post'
                                            ]);
                                        }
                                    },
                                    'kardex' => function ($url, $model, $key) {
                                        if (Yii::$app->user->can('Doctor') || Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) {
                                            return Html::a(
                                                '<i class="material-icons">content_paste</i>',
                                                ['kardex', 'id' => $model->id, 'tipo' => 'paciente'],
                                                [
                                                    'class' => 'btn btn-primary btn-round btn-just-icon',
                                                    'title' => 'Kardex',
                                                    'aria-label' => 'Kardex',
                                                    'data-pjax' => 0
                                                ]
                                            );
                                        }
                                    },
                                ]
                            ],
                        ],
                        'bordered' => true,
                        'striped' => false,
                        'hover' => true,
                        'condensed' => false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'resizableColumns' => false,
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</div>