<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receta';
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex','id'=>$kardex->paciente_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paciente-index">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left">
                            <h4>
                                <?= Html::encode($this->title) ?>
                            </h4>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right pull-right">
                            <?php if (count($dataProvider->models) < 1) { ?>
                                <?= Html::a("<i class='material-icons'>add</i> Nuevo " . 'Receta', ['paciente/recetacreate', 'id' => $id], ['class' => 'btn btn-success']) ?>
                            <?php } ?>
                        </div>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'label' => 'Paciente',
                                'attribute' => 'kardex.paciente.nombrecompleto'
                            ],
                            'jc',
                            'fecha',

                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to([$action, 'id' => $key]);
                                },
                                'width' => '200px',
                                'template' => '{update} {detallereceta} {recetapdf}',
                                'buttons' => [
                                    'update' => function ($url, $model, $key) {
                                        return Html::a(
                                            '<i class="material-icons">edit</i>',
                                            ['/paciente/recetaupdate', 'id' => $model->id],
                                            [
                                                'class' => 'btn btn-warning btn-just-icon btn-round',
                                                'title' => 'Modificar',
                                                'aria-label' => 'Modificar',
                                                'data-pjax' => 0
                                            ]
                                        );
                                    },
                                    'detallereceta' => function ($url, $model, $key) {
                                        return Html::a(
                                            '<i class="material-icons">content_paste</i>',
                                            ['/paciente/recetadetalle', 'id' => $model->id],
                                            [
                                                'class' => 'btn btn-info btn-just-icon btn-round',
                                                'title' => 'Detalle receta',
                                                'aria-label' => 'Detalle receta',
                                                'data-pjax' => 0
                                            ]
                                        );
                                    },
                                    'recetapdf' => function ($url, $model, $key) {
                                        return Html::a(
                                            '<i class="material-icons">print</i>',
                                            ['/paciente/recetadetallepdf', 'id' => $model->id],
                                            [
                                                'class' => 'btn btn-primary btn-just-icon btn-round',
                                                'target' => '_blank',
                                                'title' => 'Reporte',
                                                'aria-label' => 'Reporte',
                                                'data-pjax' => 0
                                            ]
                                        );
                                    },
                                ]
                            ],
                        ],
                        'bordered' => true,
                        'striped' => false,
                        'hover' => true,
                        'condensed' => false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'resizableColumns' => false,
                    ]); 
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>