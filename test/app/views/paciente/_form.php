<?php

use yii\helpers\Html;
use kartik\file\FileInput;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paciente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nacimiento')->widget(
            DatePicker::className(),
            [
                'name' => 'check_issue_date',
                'value' => date('y-m-d', strtotime('+2 days')),
                'options' => ['placeholder' => 'Seleccione la fecha ...'],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'autoclose' => true,
                    // 'todayHighlight' => true
                ]
            ]
        );
    ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_paciente')->widget(Select2::classname(), [
            'data' => [
                'TRANSPORTISTA' => 'TRANSPORTISTA',
                'ELFEC' => 'ELFEC',
            ],
            'options' => ['placeholder' => 'Seleccione una opcion ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>

    <div class="form-group text-right">
        <?php if($tipo && !$id){ ?>
            <?=  Html::a('<i class="material-icons">clear</i> Cerrar', [$tipo.'/create', 'tipo'=>$tipo], ['class' => 'btn btn-default']) ?>
        <?php }elseif($tipo && $id){ ?>
            <?=  Html::a('<i class="material-icons">clear</i> Cerrar', [$tipo.'/update', 'tipo'=>$tipo,'id'=>$id], ['class' => 'btn btn-default']) ?>
        <?php }else{?>
            <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['index'], [
                'class' => 'btn btn-default',
                'title' => 'Cerrar',
            ]) ?>
        <?php } ?>
        <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>