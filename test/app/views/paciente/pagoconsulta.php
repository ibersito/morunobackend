<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pagos';
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex', 'id' => $kardex->paciente_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paciente-index">
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="row">
                <?php $form = ActiveForm::begin([
                    'action' => Yii::$app->urlManager->createUrl(['paciente/pagoconsultacreate', 'id' => $id]),
                    'options'=>[
                        'class' => 'disable-submit-buttons',
                    ] 
                ]); ?>
                <div class="table-responsive">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <?= $form->field($model, 'monto')->textInput(['maxlength' => true, 'type' => 'number', 'min' => '1']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <?= $form->field($model, 'fecha')->widget(
                                            DatePicker::className(),
                                            [
                                                'name' => 'fecha',
                                                'value' => date('y-m-d', strtotime('+2 days')),
                                                'options' => ['placeholder' => 'Seleccione la fecha ...'],
                                                'pluginOptions' => [
                                                    'format' => 'yyyy-mm-dd',
                                                    'autoclose' => true,
                                                    'todayHighlight' => true
                                                ]
                                            ]
                                        );
                                        ?>
                                    </div>
                                </div>
                                <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left">
                            <h4>
                                <?= Html::encode($this->title) ?>
                            </h4>
                        </div>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'label' => 'Paciente',
                                'attribute' => 'kardex.paciente.nombrecompleto',
                            ],
                            [
                                'label' => 'Tipo',
                                'attribute' => 'kardex.tipo'
                            ],
                            [
                                'label' => 'Monto Total',
                                'attribute' => 'monto'
                            ],
                            [
                                'label' => 'Fecha',
                                'attribute' => 'fecha'
                            ],

                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to([$action, 'id' => $key]);
                                },
                                'width' => '200px',
                                'template' => '{update} {detallepago}',
                                'buttons' => [
                                    'update' => function ($url, $model, $key) {
                                        return Html::a(
                                            '<i class="material-icons">edit</i>',
                                            ['/paciente/pagoconsultaupdate', 'id' => $model->id],
                                            [
                                                'class' => 'btn btn-warning btn-just-icon btn-round',
                                                'title' => 'Modificar',
                                                'aria-label' => 'Modificar',
                                                'data-pjax' => 0
                                            ]
                                        );
                                    },
                                ]
                            ],
                        ],
                        'bordered' => true,
                        'striped' => false,
                        'hover' => true,
                        'condensed' => false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'resizableColumns' => false,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>