<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kardex".
 *
 * @property int $id
 * @property int $paciente_id
 * @property string $fecha
 * @property string $tipo
 * @property int $tratamiento_id
 * @property string $detalle
 * @property string $num_doc
 * @property string $estado
 * @property int $num_pagina
 *
 * @property Paciente $paciente
 * @property TipoTratamiento $tratamiento
 * @property Pagos[] $pagos
 * @property Receta[] $recetas
 */
class Kardex extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kardex';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo','num_pagina','fecha'],'required'],
            [['paciente_id', 'tratamiento_id', 'num_pagina'], 'integer'],
            [['fecha'], 'safe'],
            [['tipo', 'estado'], 'string'],
            [['detalle'], 'string', 'max' => 1000],
            [['num_doc'], 'string', 'max' => 255],
            [['paciente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['paciente_id' => 'id']],
            [['tratamiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoTratamiento::className(), 'targetAttribute' => ['tratamiento_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paciente_id' => 'Paciente',
            'fecha' => 'Fecha',
            'tipo' => 'Tipo',
            'tratamiento_id' => 'Tratamiento',
            'detalle' => 'Detalle',
            'num_doc' => 'Número Documento',
            'estado' => 'Estado',
            'num_pagina' => 'Número Pagina',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'paciente_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTratamiento()
    {
        return $this->hasOne(TipoTratamiento::className(), ['id' => 'tratamiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos()
    {
        return $this->hasMany(Pagos::className(), ['kardex_id' => 'id']);
    }

    public function getPago()
    {
        return $this->getPagos()->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasMany(Receta::className(), ['kardex_id' => 'id']);
    }

    public function getEstados($estado)
    {
        if ($estado == 'Pendiente') {
            return 'style="background-color: #FF9D9D;"';
            // return 'style="background-color: #FF7C7C;"';
        }
        return '';
    }
}
