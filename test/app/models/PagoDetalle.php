<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pago_detalle".
 *
 * @property int $id
 * @property int $pago_id
 * @property string $monto_pago
 * @property int $cantidad
 * @property string $fecha
 *
 * @property Pagos $pago
 */
class PagoDetalle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pago_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pago_id', 'monto_pago', 'fecha'], 'required'],
            [['pago_id', 'cantidad'], 'integer'],
            [['monto_pago'], 'number'],
            [['fecha'], 'safe'],
            [['pago_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pagos::className(), 'targetAttribute' => ['pago_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pago_id' => 'Pago ID',
            'monto_pago' => 'Monto Pago',
            'cantidad' => 'Cantidad',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPago()
    {
        return $this->hasOne(Pagos::className(), ['id' => 'pago_id']);
    }
}
