<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gasto;

/**
 * GastoSearch represents the model behind the search form of `app\models\Gasto`.
 */
class GastoSearch extends Gasto
{
    public $fechaInicio;
    public $fechaFin;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'monto', 'tratamiento_id', 'cantidad'], 'integer'],
            [['nombre', 'tipo_gasto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gasto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'monto' => $this->monto,
            'tratamiento_id' => $this->tratamiento_id,
            'cantidad' => $this->cantidad,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tipo_gasto', $this->tipo_gasto]);

        return $dataProvider;
    }
    public function egresos($fechaInicio, $fechaFin, $tipo_gasto,$tratamiento)
    {
        $query = Gasto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        $query->andFilterWhere(['>=', 'fecha', $fechaInicio])
            ->andFilterWhere(['<=', 'fecha', $fechaFin])
            ->andFilterWhere(['=', 'tipo_gasto', $tipo_gasto])
            ->andFilterWhere(['=', 'tratamiento_id', $tratamiento])
            ->groupBy(['{{gasto}}.id'])
            ->all();

        return $dataProvider;
    }
}