<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalle_receta".
 *
 * @property int $id
 * @property int $receta_id
 * @property string $detalle
 * @property string $valores
 *
 * @property Receta $receta
 */
class DetalleReceta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalle_receta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['receta_id', 'detalle', 'valores'], 'required'],
            [['receta_id'], 'integer'],
            [['detalle'], 'string', 'max' => 1000],
            [['valores'], 'string', 'max' => 50],
            [['receta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Receta::className(), 'targetAttribute' => ['receta_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'receta_id' => 'Receta ID',
            'detalle' => 'Detalle',
            'valores' => 'Valores',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceta()
    {
        return $this->hasOne(Receta::className(), ['id' => 'receta_id']);
    }
}
