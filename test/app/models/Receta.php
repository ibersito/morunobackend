<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "receta".
 *
 * @property int $id
 * @property int $kardex_id
 * @property string $jc
 * @property string $fecha
 *
 * @property DetalleReceta[] $detalleRecetas
 * @property Kardex $kardex
 */
class Receta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kardex_id', 'jc', 'fecha'], 'required'],
            [['kardex_id'], 'integer'],
            [['fecha'], 'safe'],
            [['jc'], 'string', 'max' => 100],
            [['kardex_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kardex::className(), 'targetAttribute' => ['kardex_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paciente_id' => 'Paciente ID',
            'kardex_id' => 'Kardex ID',
            'jc' => 'Jc',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleRecetas()
    {
        return $this->hasMany(DetalleReceta::className(), ['receta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKardex()
    {
        return $this->hasOne(Kardex::className(), ['id' => 'kardex_id']);
    }
}
