<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PagoConsulta;

/**
 * PagoConsultaSearch represents the model behind the search form of `app\models\PagoConsulta`.
 */
class PagoConsultaSearch extends PagoConsulta
{
    public $fechaInicio;
    public $fechaFin;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kardex_id'], 'integer'],
            [['monto'], 'number'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PagoConsulta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'monto' => $this->monto,
            'fecha' => $this->fecha,
            'kardex_id' => $this->kardex_id,
        ]);

        return $dataProvider;
    }
    public function ingresos($fechaInicio,$fechaFin)
    {
        $query = PagoConsulta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false,
        ]);
        $query->andFilterWhere(['>=', 'fecha', $fechaInicio])
            ->andFilterWhere(['<=', 'fecha', $fechaFin])
            ->groupBy(['{{pago_consulta}}.id'])
            ->all();

        return $dataProvider;
    }
}
