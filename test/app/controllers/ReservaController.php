<?php

namespace app\controllers;

use Yii;
use app\models\Kardex;
use app\models\Reserva;
use yii\web\Controller;
use app\models\Paciente;
use yii\filters\VerbFilter;
use app\models\ReservaSearch;
use app\components\PFunciones;
use yii\web\NotFoundHttpException;

/**
 * ReservaController implements the CRUD actions for Reserva model.
 */
class ReservaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reserva models.
     * @return mixed
     */
    public function actionIndex()
    {
        PFunciones::edad(Paciente::find()->all());

        $searchModel = new ReservaSearch();
        $searchModel->fecha = date('d-m-Y');
        // $searchModel->num_ficha = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,false);

        $dataProvider->setSort([
            'defaultOrder' => ['hora' => SORT_ASC],
        ]);

        $dataProvider2 = $searchModel->search(Yii::$app->request->queryParams,true);

        $dataProvider2->setSort([
            'defaultOrder' => ['num_ficha' => SORT_ASC],
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    /**
     * Displays a single Reserva model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reserva model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tipo,$paciente_id = null)
    {
        $model = new Reserva();
        $model->paciente_id = $paciente_id;
        $model->fecha = date('d-m-Y');
        $request = Yii::$app->request;
        if ($model->load($request->post())) {
            
            //$maxficha = Reserva::find()->where(['fecha'=> $request->post('Reserva')['fecha']])->max('num_ficha');
            $hora = Reserva::find()->where(['hora'=>$request->post('Reserva')['hora'],'fecha'=>$request->post('Reserva')['fecha']])->exists();
            $paciente = Reserva::find()->where(['paciente_id' => $request->post('Reserva')['paciente_id'], 'fecha' => $request->post('Reserva')['fecha']])->exists();
            if($hora || $paciente){
                Yii::$app->session->setFlash('error', 'Ya se encuentra el usuario y/o horario asignado');
                return $this->redirect(['create','tipo'=>$tipo]);
            }

            $model->num_ficha = 0;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'tipo' => $tipo
        ]);
    }

    /**
     * Updates an existing Reserva model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($tipo,$id, $paciente_id = null)
    {
        $model = $this->findModel($id);
        // $tipo = 'reserva';
        $model->paciente_id = $paciente_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'tipo' => $tipo,
        ]);
    }

    public function actionFicha($id)
    {

        $pacc = Reserva::find()->where(['fecha' => date('d-m-Y'), 'id' => $id])->one();
        if ($pacc->num_ficha > 0) {
            Yii::$app->session->setFlash('error', 'El paciente ya tiene una ficha asignada');
            return $this->redirect(['index']);
        }
        // $model = new Reserva();
        // $pacientes = Reserva::find()->where(['fecha' => date('Y-m-d')])->andWhere(['!=', 'paciente_id', $id])->andWhere(['>=', 'num_ficha', 1])->all();
        // $paciente = Reserva::find()->where(['paciente_id' => $id, 'fecha' => date('Y-m.d')])->one();
        $pacienteultimaficha = Reserva::find()->where(['fecha' => date('Y-m.d')])->andWhere(['!=', 'id', $id])->andWhere(['>=', 'num_ficha', 1])->orderBy(["num_ficha"=> SORT_DESC])->one();
        // $pacc = Reserva::find()->where(['id' => $id])->one();
        if ($pacienteultimaficha) {
            $pacc->num_ficha = $pacienteultimaficha->num_ficha+1;
        }else{
            $pacc->num_ficha = 1;
        }
        $pacc->save();
        return $this->redirect(['index']);
        // return $this->render('ficha', [
        //     'model' => $model,
        //     'paciente' => $paciente,
        //     'pacientes' => $pacientes,
        // ]);




        // $existe = Reserva::find()->where(['fecha' => date('Y-m-d'), 'paciente_id' => $id])->one();
        // if ($existe->num_ficha == NULL) {
        //     Yii::$app->session->setFlash('error', 'El paciente no tiene un numero de ficha asignado');
        //     return $this->redirect(['index']);
        // }
        // $model = new Reserva();
        // $pacientes = Reserva::find()->where(['fecha' => date('Y-m-d')])->andWhere(['!=', 'paciente_id', $id])->andWhere(['>=', 'num_ficha', 1])->all();
        // $paciente = Reserva::find()->where(['paciente_id' => $id, 'fecha' => date('Y-m.d')])->one();

        // $request = Yii::$app->request;
        // if ($request->isPost) {
        //     $paca = Reserva::find()->where(['paciente_id' => $id, 'fecha' => date('Y-m-d')])->one();
        //     $pacc = Reserva::find()->where(['paciente_id' => $request->post('Reserva')['paciente_id'], 'fecha' => date('Y-m.d')])->one();
        //     //ficha paciente actual
        //     $fichapaca = $paca->num_ficha;
        //     //ficha paciente seleccioando
        //     $fichapacc = $pacc->num_ficha;

        //     //cambio de ficha al paciente elegido
        //     $pacc->num_ficha = $fichapaca;
        //     //cambio de ficha al paciente actual
        //     $paca->num_ficha = $fichapacc;

        //     //guardamos las fichas cambiadas
        //     $pacc->save();
        //     $paca->save();

        //     return $this->redirect(['index']);
        // }

        // return $this->render('ficha', [
        //     'model' => $model,
        //     'paciente' => $paciente,
        //     'pacientes' => $pacientes,
        // ]);
    }

    public function actionFicharemove($id)
    {
        $pacc = Reserva::find()->where(['fecha' => date('d-m-Y'), 'id' => $id])->one();
        $pacc->num_ficha = 0;
        $pacc->save();
        return $this->redirect(['index']);
    }

    public function actionNumficha($id)
    {
        $reserva = Reserva::find()->where(['paciente_id' => $id, 'fecha' => date('d-m-Y')])->one();
        $reservacount = Reserva::find()->where(['paciente_id' => $id, 'fecha' => date('d-m-Y')])->count();

        if ($reservacount > 0) {
            echo "<option value='" . $reserva->num_ficha . "'>" . $reserva->num_ficha . "</option>";
        } else {
            echo "<option value='0'></option>";
        }
    }

    /**
     * Deletes an existing Reserva model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        // PFunciones::ficha(Reserva::find()->where(['fecha' => date('Y-m-d')])->all());
        return $this->redirect(['index']);
    }

    /**
     * Finds the Reserva model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reserva the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reserva::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
