<?php

namespace app\controllers;

use app\models\Gasto;
use Yii;
use kartik\mpdf\Pdf;
use app\models\Kardex;
use yii\data\Pagination;
use yii\base\DynamicModel;
use app\models\GastoSearch;
use app\models\KardexSearch;
use app\models\PagoConsulta;
use app\models\PagoConsultaSearch;
use app\models\PagoDetalle;
use app\models\PagoDetalleSearch;

class ReporteController extends \yii\web\Controller
{
    public function actionIngresos()
    {
        $request = Yii::$app->request;
        $searchModel = new PagoDetalleSearch();
        $searchModel->fechaInicio = date('Y-m-d');
        $searchModel->fechaFin = date('Y-m-d');
        $searchModel->tipo = "";
        if ($request->isPost) {
            $searchModel->fechaInicio = $request->post('PagoDetalleSearch')['fechaInicio'];
            $searchModel->fechaFin = $request->post('PagoDetalleSearch')['fechaFin'];
            $searchModel->tipo = $request->post('PagoDetalleSearch')['tipo'];            
        }
        if($searchModel->tipo == 'Todo'){
            $ingresos = $searchModel->todos(Yii::$app->request->queryParams);
        }else{
            $ingresos = $searchModel->ingresos($searchModel->fechaInicio, $searchModel->fechaFin, $searchModel->tipo);
        }
        $ingresos->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        // $ingresos = $searchModel->ingresos($searchModel->fechaInicio, $searchModel->fechaFin, $searchModel->tipo);
        return $this->render('ingresos', [
            'model' => $searchModel,
            'ingresos' => $ingresos,
            'fechaInicio' => $searchModel->fechaInicio,
            'fechaFin' => $searchModel->fechaFin,
            'tipo' => $searchModel->tipo,
        ]);
    }    
    public function actionReporteingresospdf($fechaInicio,$fechaFin,$tipo)
    {
        //realizando la consulta con las fechas recibidas
        $searchModel = new PagoDetalleSearch();
        if($tipo == 'Todo'){
            $ingresos = $searchModel->todos(Yii::$app->request->queryParams);
        }else{
            $ingresos = $searchModel->ingresos($fechaInicio,$fechaFin,$tipo);
        }
        //contenido html a transformar en PDF
        $content = $this->renderPartial('reporteingresospdf', [
            'ingresos' => $ingresos,
        ], true);
        //configurando pagina PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        //generando PDF
        return $pdf->render();
    }
    public function actionIngresosconsulta()
    {
        $request = Yii::$app->request;
        $searchModel = new PagoConsultaSearch();
        $searchModel->fechaInicio = date('Y-m-d');
        $searchModel->fechaFin = date('Y-m-d');
        if ($request->isPost) {
            $searchModel->fechaInicio = $request->post('PagoConsultaSearch')['fechaInicio'];
            $searchModel->fechaFin = $request->post('PagoConsultaSearch')['fechaFin'];
        }
        $ingresos = $searchModel->ingresos($searchModel->fechaInicio, $searchModel->fechaFin);
        $ingresos->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        return $this->render('ingresosconsulta', [
            'model' => $searchModel,
            'ingresos' => $ingresos,
            'fechaInicio' => $searchModel->fechaInicio,
            'fechaFin' => $searchModel->fechaFin,
        ]);
    }
    public function actionReporteingresosconsultapdf($fechaInicio, $fechaFin)
    {
        //realizando la consulta con las fechas recibidas
        $searchModel = new PagoConsultaSearch();
        $ingresos = $searchModel->ingresos($fechaInicio, $fechaFin);
        //contenido html a transformar en PDF
        $content = $this->renderPartial('reporteingresosconsultapdf', [
            'ingresos' => $ingresos,
        ], true);
        //configurando pagina PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        //generando PDF
        return $pdf->render();
    }
    public function actionEgresos()
    {
        $request = Yii::$app->request;
        $searchModel = new GastoSearch();
        $searchModel->fechaInicio = date('Y-m-d');
        $searchModel->fechaFin = date('Y-m-d');
        $searchModel->tipo_gasto = "";
        $searchModel->tratamiento_id = "";
        if ($request->isPost) {
            $searchModel->fechaInicio = $request->post('GastoSearch')['fechaInicio'];
            $searchModel->fechaFin = $request->post('GastoSearch')['fechaFin'];
            $searchModel->tipo_gasto = $request->post('GastoSearch')['tipo_gasto'];
            $searchModel->tratamiento_id = $request->post('GastoSearch')['tratamiento_id'];
        }
        $egresos = $searchModel->egresos($searchModel->fechaInicio, $searchModel->fechaFin, $searchModel->tipo_gasto, $searchModel->tratamiento_id);
        $egresos->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        return $this->render('egresos', [
            'model' => $searchModel,
            'egresos' => $egresos,
            'fechaInicio' => $searchModel->fechaInicio,
            'fechaFin' => $searchModel->fechaFin,
            'tipo_gasto' => $searchModel->tipo_gasto,
            'tratamiento' => $searchModel->tratamiento_id,
        ]);
    }
    public function actionReporteegresospdf($fechaInicio, $fechaFin, $tipo_gasto, $tratamiento)
    {
        //realizando la consulta con las fechas recibidas
        $searchModel = new GastoSearch();
        $egresos = $searchModel->egresos($fechaInicio, $fechaFin, $tratamiento, $tipo_gasto, $tratamiento);
        //contenido html a transformar en PDF
        $content = $this->renderPartial('reporteegresospdf', [
            'egresos' => $egresos,
        ], true);
        //configurando pagina PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        //generando PDF
        return $pdf->render();
    }
    public function actionArqueo()
    {
        $request = Yii::$app->request;

        $search = new DynamicModel([
            'fecha',
        ]);
        $search->addRule(['fecha'], 'safe');
        $search->fecha = date('Y-m-d');

        if ($request->isPost) {
            $search->fecha = $request->post('DynamicModel')['fecha'];
        }

        $arqueoingreso = PagoDetalle::find()
            ->leftJoin('pagos','pago_detalle.pago_id = pagos.id')
            ->leftJoin('kardex','pagos.kardex_id = kardex.id')
            ->select('kardex.tipo,pago_detalle.monto_pago')
            ->andFilterWhere(['=', 'pago_detalle.fecha', $search->fecha])
            ->orderBy(['pago_detalle.id' => SORT_DESC])
            ->asArray()
            ->all();

        $arqueoegreso = Gasto::find()
            ->andFilterWhere(['=', 'fecha', $search->fecha])
            ->orderBy(['id' => SORT_DESC])
            ->asArray()
            ->all();
        
        $arqueoingresoconsulta = PagoConsulta::find()
            ->leftJoin('kardex','pago_consulta.kardex_id = kardex.id')
            ->select('kardex.tipo,pago_consulta.monto')
            ->andFilterWhere(['=', 'pago_consulta.fecha', $search->fecha])
            ->orderBy(['pago_consulta.id' => SORT_DESC])
            ->asArray()
            ->all();
         
        return $this->render('arqueo', [
            'model' => $search,
            'arqueoingreso' => $arqueoingreso,
            'arqueoingresoconsulta' => $arqueoingresoconsulta,
            'arqueoegreso' => $arqueoegreso,
            'fecha' => $search->fecha,
        ]);
    }
    public function actionReportearqueopdf($fecha)
    {
        $arqueoingreso = PagoDetalle::find()
            ->leftJoin('pagos', 'pago_detalle.pago_id = pagos.id')
            ->leftJoin('kardex', 'pagos.kardex_id = kardex.id')
            ->select('kardex.tipo,pago_detalle.monto_pago')
            ->andFilterWhere(['=', 'pago_detalle.fecha', $fecha])
            ->orderBy(['pago_detalle.id' => SORT_DESC])
            ->asArray()
            ->all();

        $arqueoingresoconsulta = PagoConsulta::find()
            ->leftJoin('kardex', 'pago_consulta.kardex_id = kardex.id')
            ->select('kardex.tipo,pago_consulta.monto')
            ->andFilterWhere(['=', 'pago_consulta.fecha', $fecha])
            ->orderBy(['pago_consulta.id' => SORT_DESC])
            ->asArray()
            ->all();

        $arqueoegreso = Gasto::find()
            ->andFilterWhere(['=', 'fecha', $fecha])
            ->orderBy(['id' => SORT_DESC])
            ->asArray()
            ->all();

        $content =  $this->renderPartial('reportearqueopdf', [
            'arqueoingresoconsulta' => $arqueoingresoconsulta,
            'arqueoingreso' => $arqueoingreso,
            'arqueoegreso' => $arqueoegreso,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);

        return $pdf->render();
    }
    public function actionTratamientos()
    {
        $request = Yii::$app->request;
        $searchModel = new KardexSearch();
        $searchModel->estado = 'Activo';
        $searchModel->fechaInicio = date('Y-m-d');
        $searchModel->fechaFin = date('Y-m-d');
        $searchModel->telefono = "";
        $searchModel->tipo_paciente = "";
        if ($request->isPost) {
            $searchModel->fechaInicio = $request->post('KardexSearch')['fechaInicio'];
            $searchModel->fechaFin = $request->post('KardexSearch')['fechaFin'];
            $searchModel->tratamiento_id = $request->post('KardexSearch')['tratamiento_id'];
            $searchModel->paciente_id = $request->post('KardexSearch')['paciente_id'];
            $searchModel->telefono = $request->post('KardexSearch')['telefono'];
            $searchModel->tipo_paciente = $request->post('KardexSearch')['tipo_paciente'];
        }
        $tratamientos = $searchModel->tratamientos($searchModel->fechaInicio,$searchModel->fechaFin,$searchModel->tratamiento_id,$searchModel->paciente_id,$searchModel->telefono, $searchModel->tipo_paciente);

        return $this->render('tratamientos', [
            'model' => $searchModel,
            'tratamientos' => $tratamientos,
            'fechaInicio' => $searchModel->fechaInicio,
            'fechaFin' => $searchModel->fechaFin,
            'tratamiento' => $searchModel->tratamiento_id,
            'paciente' => $searchModel->paciente_id,
            'telefono' => $searchModel->telefono,
            'tipo_paciente' => $searchModel->tipo_paciente,

        ]);
    }
    public function actionReportetratamientospdf($fechaInicio, $fechaFin, $tratamiento,$paciente,$telefono,$tipo_paciente)
    {
        //realizando la consulta con las fechas recibidas
        $searchModel = new KardexSearch();
        $tratamientos = $searchModel->tratamientos($fechaInicio, $fechaFin, $tratamiento,$paciente,$telefono,$tipo_paciente);
        //contenido html a transformar en PDF
        $content = $this->renderPartial('reportetratamientospdf', [
            'tratamientos' => $tratamientos,
        ], true);
        //configurando pagina PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        //generando PDF
        return $pdf->render();
    }
    public function actionSaldos()
    {
        $request = Yii::$app->request;
        $searchModel = new KardexSearch();
        $searchModel->estado = 'Pendiente';
        $searchModel->fechaInicio = date('Y-m-d');
        $searchModel->fechaFin = date('Y-m-d');
        $searchModel->telefono = "";
        if ($request->isPost) {
            $searchModel->fechaInicio = $request->post('KardexSearch')['fechaInicio'];
            $searchModel->fechaFin = $request->post('KardexSearch')['fechaFin'];
            $searchModel->tratamiento_id = $request->post('KardexSearch')['tratamiento_id'];
            $searchModel->paciente_id = $request->post('KardexSearch')['paciente_id'];
            $searchModel->telefono = $request->post('KardexSearch')['telefono'];
        }
        $saldos = $searchModel->saldos($searchModel->fechaInicio, $searchModel->fechaFin, $searchModel->tratamiento_id, $searchModel->paciente_id, $searchModel->telefono);
        return $this->render('saldos', [
            'model' => $searchModel,
            'saldos' => $saldos,
            'fechaInicio' => $searchModel->fechaInicio,
            'fechaFin' => $searchModel->fechaFin,
            'tratamiento' => $searchModel->tratamiento_id,
            'paciente' => $searchModel->paciente_id,
            'telefono' => $searchModel->telefono,

        ]);
    }
    public function actionReportesaldospdf($fechaInicio, $fechaFin, $tratamiento, $paciente, $telefono)
    {
        //realizando la consulta con las fechas recibidas
        $searchModel = new KardexSearch();
        $saldos = $searchModel->saldos($fechaInicio, $fechaFin, $tratamiento, $paciente, $telefono);
        //contenido html a transformar en PDF
        $content = $this->renderPartial('reportesaldospdf', [
            'saldos' => $saldos,
        ], true);
        //configurando pagina PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        //generando PDF
        return $pdf->render();
    }
}