<?php

namespace app\controllers;

use Yii;
use app\models\Team;
use app\models\Zona;
use yii\web\Response;
use app\models\Cliente;
use app\models\FuiLona;
use yii\web\Controller;
use app\models\Contacto;
use app\models\Nosotros;
use app\models\Paciente;
use app\models\Servicio;
use yii\data\Pagination;
use app\models\Portafolio;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
// use app\models\ContactForm;
// use app\models\AsistenciaSeminario;
// use app\models\Estudiante;
// use app\models\AsignacionCasillero;
// use app\models\Mensualidad;
// use app\models\Year;
// use app\models\Inscripcion;
// use app\models\Baner;
// use app\models\Ambiente;
// use app\models\MenuComida;
// use app\models\Evento;
use app\models\EspacioPublicitario;
use app\models\TipoEspacioPublicitario;
use app\components\errors\ErrorsComponent;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'index', 
                            // 'error',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except'=>[
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @param $id id de zona
     * @return string
     */
    public function actionIndex($id = null)
    {
        $pacientes = Paciente::find()->all();
        foreach ($pacientes as $paciente) {
            $model = Paciente::findOne($paciente->id);
            $model->edad = date("Y") - date("Y", strtotime($paciente->fecha_nacimiento));
            $model->save();
        }
        
        return $this->render('index');
    }


    // public function actionError(){
    //     // $this->layout = 'website';
    //     return $this->render('error');
    // }
}
