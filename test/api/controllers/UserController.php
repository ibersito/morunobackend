<?php

namespace app\controllers;

use app\models\User;
use app\models\Login;
use yii\filters\Cors;
use app\models\Reserva;

use app\models\Paciente;
use yii\data\Pagination;
use yii\rest\Controller;
use app\models\Emergencia;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\TipoTratamiento;
use yii\web\UnauthorizedHttpException;
use app\components\errors\ErrorsComponent;
use app\components\PFunciones;
use yii\web\UnprocessableEntityHttpException;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'corsFilter'  => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => false,
                    'Access-Control-Max-Age' => 3600,
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],
            // 'bearerAuth' => [
            //     'class' => HttpBearerAuth::className(),
            //     'except'=>[
            //         'index',
            //         'signup',
            //         'login',
            //         // 'perfil',
            //     ]
            // ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'login' => ['post'],
                ],
            ],
        ];
    }

    private function checkAccess($action, $model = null, $params = [])
    {
    }
    /**
     * Accion de la pagina principal
     * @return JSON
     */
    public function actionIndex()
    {
        return "Welcome to API's MEDICO";
    }
    public function actionLogin()
    {
        $request = \Yii::$app->request;
        if ($request->post('password')) {
            // login normal
            $login = new Login();
            $login->email = $request->post('email');
            $login->password = $request->post('password');
            if ($login->login()) {
                $usuario = User::find()
                    ->where([
                        'id' => $login->user->id
                    ])
                    ->one();
                return $usuario;
            } else {
                throw new UnauthorizedHttpException("El usuario no es valido");
            }
        }
    }
    public function actionPacientes()
    {
        $request = \Yii::$app->request;
        $query = Paciente::find();
        $page = $request->get('page');
        $pagination = new Pagination(['totalCount' => $query->count()]);
        $pagination->setPageSize($request->get('page_size'));
        $pacientes = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $datos = ArrayHelper::toArray($pacientes, [
            Paciente::className() => [
                'id',
                'nombres',
                'apellidos',
                'fecha_nacimiento',
                'edad',
                'telefono',
                'celular'
            ],
        ]);

        if ($page >= 1 && $page <= $pagination->getPageCount()) {
            return $datos;
        } else {
            return [];
        }
    }
    public function actionSearchpaciente()
    {
        $request = \Yii::$app->request;
        return Paciente::find()
                ->andFilterWhere(['like','nombres', $request->post('nombre')])
                ->orFilterWhere(['like','apellidos',$request->post('nombre')])
                ->all();
    }
    public function actionTratamientos()
    {
        return TipoTratamiento::find()->all();
    }
    public function actionReservas()
    {
        $reservas = Reserva::find()->where(['fecha' => date('d-m-Y')])->orderBy(['hora' => SORT_ASC])->all();

        $datos = ArrayHelper::toArray($reservas, [
            Reserva::className() => [
                'id',
                'fecha',
                'hora',
                'hora_ingreso',
                'hora_llegada',
                'tipo',
                'num_ficha',
                'observacion',
                'tratamiento' => function ($model) {
                    return $model->tratamiento->nombre;
                },
                'paciente' => function ($model) {
                    return[
                        'id' => $model->paciente->id,
                        'nombre' => $model->paciente->nombrecompleto,
                        'nombres' => $model->paciente->nombres,
                        'apellidos' => $model->paciente->apellidos,
                        'fecha_nacimiento' => $model->paciente->fecha_nacimiento,
                        'edad' => $model->paciente->edad,
                        'telefono' => $model->paciente->telefono,
                        'celular' => $model->paciente->celular,
                    ];
                },
            ],
        ]);

        if ($datos) {
            return $datos;
        } else {
            return [];
        }
    }
    public function actionReservassearch()
    {
        $request = \Yii::$app->request;
        $dateRealDate = date("d-m-Y", strtotime($request->post('fecha')));  
        $reservas = Reserva::find()->andFilterWhere(['like','fecha',$dateRealDate])->orderBy(['hora' => SORT_ASC])->all();

        $datos = ArrayHelper::toArray($reservas, [
            Reserva::className() => [
                'id',
                'fecha',
                'hora',
                'hora_ingreso',
                'hora_llegada',
                'tipo',
                'num_ficha',
                'observacion',
                'tratamiento' => function ($model) {
                    return $model->tratamiento->nombre;
                },
                'paciente' => function ($model) {
                    return [
                        'id' => $model->paciente->id,
                        'nombre' => $model->paciente->nombrecompleto,
                        'nombres' => $model->paciente->nombres,
                        'apellidos' => $model->paciente->apellidos,

                        'fecha_nacimiento' => $model->paciente->fecha_nacimiento,
                        'edad' => $model->paciente->edad,
                        'telefono' => $model->paciente->telefono,
                        'celular' => $model->paciente->celular,
                    ];
                },
            ],
        ]);

        if ($datos) {
            return $datos;
        } else {
            return [];
        }
    }
    public function actionCrearreserva()
    {
        $request = \Yii::$app->request;
        $reserva = new Reserva();
        //$maxficha = Reserva::find()->where(['fecha' => $request->post('fecha')])->max('num_ficha');
        if ($request->isPost) {
            $transaction = $reserva->getDb()->beginTransaction();
            try {
                $reserva->paciente_id = $request->post('paciente_id');

                $reserva->fecha  = date("d-m-Y", strtotime($request->post('fecha')));  
               
                // $reserva->fecha = $request->post('fecha');
                $reserva->hora = $request->post('hora');
                $reserva->tipo = $request->post('tipo');
                $reserva->num_ficha = 0;
                $reserva->tratamiento_id = $request->post('tratamiento_id');
                $reserva->observacion = $request->post('observacion');
                if (!$reserva->validate()) {
                    throw new \Exception(ErrorsComponent::formatJustString($reserva->errors));
                }
                $value = $reserva->save();
                if (!$value) {
                    return $reserva->errors;
                }
                $transaction->commit();
                return Reserva::find()
                    ->where([
                        'id' => $reserva->id
                    ])
                    ->asArray()
                    ->one();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw new UnprocessableEntityHttpException($e->getMessage());
            }
        }
        ErrorsComponent::format($reserva->errors);
    }
    public function actionEditarreserva()
    {
        $request = \Yii::$app->request;
        $reserva = Reserva::findOne($request->post('reserva_id'));
        if ($request->isPost) {
            $transaction = $reserva->getDb()->beginTransaction();
            try {
                //$reserva->hora_ingreso = $request->post('hora_ingreso');
                //$reserva->hora_llegada = $request->post('hora_llegada');
                $reserva->paciente_id = $request->post('paciente_id');
                $reserva->hora = $request->post('hora');
                $reserva->fecha  = date("d-m-Y", strtotime($request->post('fecha')));  
                // $reserva->fecha = $request->post('fecha');

                if (!$reserva->validate()) {
                    throw new \Exception(ErrorsComponent::formatJustString($reserva->errors));
                }
                $value = $reserva->save();
                if (!$value) {
                    return $reserva->errors;
                }
                $transaction->commit();
                return Reserva::find()
                    ->where([
                        'id' => $reserva->id
                    ])
                    ->asArray()
                    ->one();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw new UnprocessableEntityHttpException($e->getMessage());
            }
        }
        ErrorsComponent::format($reserva->errors);
    }
    public function actionEliminarreserva()
    {
        $request = \Yii::$app->request;
        $reserva = Reserva::findOne($request->post('reserva_id'));
        $reserva->delete();
        //PFunciones::ficha(Reserva::find()->where(['fecha' => date('Y-m-d')])->all());
        \Yii::$app->response->statusCode = 200;
        return array(
            'status' => true,
            'data' => 'La reserva se elimino correctamente'
        );
    }
    public function actionEmergencias()
    {
        $emergencias = Emergencia::find()->where(['fecha'=>date('d-m-Y')])->orderBy(['hora' => SORT_ASC])->all();

        $datos = ArrayHelper::toArray($emergencias, [
            Emergencia::className() => [
                'id',
                'fecha',
                'hora',
                'hora_ingreso',
                'tipo',
                'observacion',
                'tratamiento' => function ($model) {
                    return $model->tratamiento->nombre;
                },
                'paciente' => function ($model) {
                    return [
                        'id' => $model->paciente->id,
                        'nombre' => $model->paciente->nombrecompleto,
                        'nombres' => $model->paciente->nombres,
                        'apellidos' => $model->paciente->apellidos,

                        'fecha_nacimiento' => $model->paciente->fecha_nacimiento,
                        'edad' => $model->paciente->edad,
                        'telefono' => $model->paciente->telefono,
                        'celular' => $model->paciente->celular,
                    ];
                },
            ],
        ]);

        if ($datos) {
            return $datos;
        } else {
            return [];
        }
    }
    public function actionEmergenciassearch()
    {
        $request = \Yii::$app->request;
        $dateRealDate = date("d-m-Y", strtotime($request->post('fecha')));  

        $emergencias = Emergencia::find()->andFilterWhere(['=', 'fecha', $dateRealDate])->orderBy(['hora' => SORT_ASC])->all();

        $datos = ArrayHelper::toArray($emergencias, [
            Emergencia::className() => [
                'id',
                'fecha',
                'hora',
                'hora_ingreso',
                'tipo',
                'observacion',
                'tratamiento' => function ($model) {
                    return $model->tratamiento->nombre;
                },
                'paciente' => function ($model) {
                    return [
                        'id' => $model->paciente->id,
                        'nombre' => $model->paciente->nombrecompleto,
                        'nombres' => $model->paciente->nombres,
                        'apellidos' => $model->paciente->apellidos,

                        'fecha_nacimiento' => $model->paciente->fecha_nacimiento,
                        'edad' => $model->paciente->edad,
                        'telefono' => $model->paciente->telefono,
                        'celular' => $model->paciente->celular,
                    ];
                },
            ],
        ]);

        if ($datos) {
            return $datos;
        } else {
            return [];
        }
    }
    public function actionCrearemergencia()
    {
        $request = \Yii::$app->request;
        $emergencia = new Emergencia();
        if ($request->isPost) {
            $transaction = $emergencia->getDb()->beginTransaction();
            try {
                $emergencia->paciente_id = $request->post('paciente_id');
                $emergencia->fecha  = date("d-m-Y", strtotime($request->post('fecha')));  
                // $emergencia->fecha = $request->post('fecha');
                $emergencia->hora = $request->post('hora');
        
                $emergencia->tratamiento_id = $request->post('tratamiento_id');
                $emergencia->observacion = $request->post('observacion');
                if (!$emergencia->validate()) {
                    throw new \Exception(ErrorsComponent::formatJustString($emergencia->errors));
                }
                $value = $emergencia->save();
                if (!$value) {
                    return $emergencia->errors;
                }
                $transaction->commit();
                return Emergencia::find()
                    ->where([
                        'id' => $emergencia->id
                    ])
                    ->asArray()
                    ->one();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw new UnprocessableEntityHttpException($e->getMessage());
            }
        }
        ErrorsComponent::format($emergencia->errors);
    }
    public function actionEditaremergencia()
    {
        $request = \Yii::$app->request;
        $emergencia = Emergencia::findOne($request->post('emergencia_id'));
        if ($request->isPost) {
            $transaction = $emergencia->getDb()->beginTransaction();
            try {
                $emergencia->paciente_id = $request->post('paciente_id');
                $emergencia->fecha  = date("d-m-Y", strtotime($request->post('fecha')));  

                // $emergencia->fecha = $request->post('fecha');
                $emergencia->hora = $request->post('hora');
                if (!$emergencia->validate()) {
                    throw new \Exception(ErrorsComponent::formatJustString($emergencia->errors));
                }
                $value = $emergencia->save();
                if (!$value) {
                    return $emergencia->errors;
                }
                $transaction->commit();
                return emergencia::find()
                    ->where([
                        'id' => $emergencia->id
                    ])
                    ->asArray()
                    ->one();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw new UnprocessableEntityHttpException($e->getMessage());
            }
        }
        ErrorsComponent::format($emergencia->errors);
    }
    public function actionEliminaremergencia()
    {
        $request = \Yii::$app->request;
        $emergencia = Emergencia::findOne($request->post('emergencia_id'));
        $emergencia->delete();
        \Yii::$app->response->statusCode = 200;
        return array(
            'status' => true,
            'data' => 'La emergencia se elimino correctamente'
        );
    }
}