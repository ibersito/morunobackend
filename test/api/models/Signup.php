<?php
namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Signup form
 */
class Signup extends Model
{
    // /**
    //  * @var string funciona como username de usuario
    //  */
    // public $username;

    /**
     * @var string email de ususario
     */
    public $email;

    /**
     * @var string password del ususario
     */
    public $password;

    /**
     * @var string nombre completo del usuario
     */
    public $nombre_completo;

    /**
     * @var string celular del usuario
     */
    public $celular;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'El email ya existe.'],

            ['password', 'required'],
            ['password', 'string'],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->nombre_completo = $this->nombre_completo;
            $user->email = $this->email;
            $user->celular = $this->celular;
            $user->generateAuthKey();
            $user->setPassword($this->password);
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
    

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
