   var cont = 0;
   var valid = false;
   total = 0;
   subtotal = [];
   $("#cancelar").show();
   $("#guardar").hide();
   $("#pidarticulo").change(mostrarValores);

   function mostrarValores() {
       datosArticulo = document.getElementById('pidarticulo').value.split('_');
       $("#pprecio_venta").val(datosArticulo[2]);
       $("#pstock").val(datosArticulo[1]);
   }

   function agregar(objProducto = null) {
       var datosArticulo = document.getElementById('pidarticulo').value.split('_');
       var idarticulo = datosArticulo[0];
       var articulo = $("#pidarticulo option:selected").text();
        if (objProducto != null){
            idarticulo = objProducto['id'];
            articulo = objProducto['producto']['nombre'];
        }
       var cantidad = $("#pcantidad").val();
       var precio_venta = $("#pprecio_venta").val();
       var stock = $("#pstock").val();

       if (idarticulo != "" && cantidad != "" && cantidad > 0 && precio_venta != "") {
           if (Number(stock) >= Number(cantidad)) {
               if ( existProductoInDetail(idarticulo) == false ){
                   subtotal[cont] = (cantidad * precio_venta);
                   total = total + subtotal[cont];
                   var fila = '<tr class="selected" id="fila' + cont + '">\
                        <td>\
                            <input type="hidden" name="idarticulo[]"  value="' + idarticulo + '">' + articulo + '\
                        </td>\
                        <td> \
                            <input  class="txt-cantidad" type="hidden" name="cantidad[]"  value="' + cantidad + '">\
                            <span class="span-cantidad">' + cantidad + '</span>\
                        </td>\
                        <td>\
                            <input type="hidden" name="precio_venta[]"  value="' + precio_venta + '">' + precio_venta + '\
                        </td>\
                        <td>\
                            <button type="button" class="btn btn-danger" onclick="eliminar(' + cont + ');">X</button>\
                        </td>\
                        <td>' + subtotal[cont] + '</td>\
                    </tr>';
                   cont++;
                   limpiar();
                   $('#total').html("Bs/ " + total);
                   $('#total_venta').val(total);
                   evaluar();
                   $('#detalles').append(fila);
               }
           } else {
               limpiar();
               alert('La cantidad a vender supera el stock');
           }

       } else {
           limpiar();
           alert("Error al ingresar el detalle de la venta, revise los datos del producto");
       }
       limpiar();
   }

   function limpiar() {
    //    $("#pcantidad").val("");
       $("#input-codebar").val("");
       $("#pprecio_venta").val("");
       $("#pstock").val("");
       $("#pidarticulo").val("a");
   }

   function evaluar() {
       if (total > 0) {
           $("#guardar").show();
           $("#cancelar").hide();
       } else {
           $("#guardar").hide();
           $("#cancelar").show();
       }
   }

   function eliminar(index) {
       total = total - subtotal[index];
       $('#total').html("Bs/. " + total);
       $('#total_venta').val(total);
       $('#fila' + index).remove();
       evaluar();
   }


   $("#w0").submit(function (e) {
       if (!valid) {
           e.preventDefault();
       }
   });

    function enviarFormulario(){
        if( window.confirm("¿ Desea realizar la venta ?") ){
            valid = true;
            $("#w0").submit();
        }else{
            valid = false;
        }
    }

   function getAlmacen(){
       var idProducto = $("#input-codebar").val();
       $.ajax({
           beforeSend: function (xhr) {
               $("#loading").css("display", "block");
           },
           complete: function (jqXHR, textStatus) {
               $("#loading").css("display", "none");
           },
           success: function (data) {
                if (data != false){
                    $("#pprecio_venta").val( data['producto']['precio'] );
                    $("#pstock").val( data['cantidad'] );
                    agregar(data);
                }
           },
           error: function (jqXHR, textStatus, errorThrown) {
               if (jqXHR.status == "403") {
                   location.reload();
               }
           },
           type: 'POST',
           dataType: "JSON",
           url: "/venta/get-almacen",
           contentType: "application/x-www-form-urlencoded; charset=UTF-8",
           data: {
               'id_producto': idProducto,
           },
       });
   }


   $('#input-codebar').keyup(function (e) {
       var enterKey = 13;
       if (e.which == enterKey) {
           getAlmacen();
       }
   });

   /**
    * Verifica si el prodycto ya ha s
    * ido agregado e el detalle
    *
    * @param int id
    * @returns
    */
   function existProductoInDetail(id) {
       var res = false;
       $('input[name="idarticulo[]"]').each(function (index, item) {
           if (item.value == id) {
               var cantidad = parseInt($(item).closest('tr').find('.txt-cantidad').eq(0).val());
               if ( (cantidad+1) <= parseInt($("#pstock").val()) ) {
                    if ( $("#pcantidad").val() != "" ){
                        cantidad += parseInt($("#pcantidad").val()); 
                    }else{
                       cantidad++;
                    }
                    $(item).closest('tr').find('.txt-cantidad').eq(0).val(cantidad);
                    $(item).closest('tr').find('.span-cantidad').html(cantidad);
                } 
               res = true;
           }
       });
       return res;
   }