$(document).ready(function () {
    $('#kardex-monto').change(function () {
        saldo();
    });
    $('#kardex-pagado').change(function () {
        saldo();
    });
});
function saldo() {
    var monto = $("#kardex-monto").val();
    var pagado = $("#kardex-pagado").val();
    
    var saldo = monto - pagado;
    $("#kardex-saldo").val(saldo);
}