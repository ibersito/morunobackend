const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

// tareas
gulp.task("sass", () => {
  gulp.src("./scss/*.scss")
    .pipe(sass({
      outputStyle:'nested',//con identacion default
      // outputStyle:'expended'//sin identacion
      // outputStyle:'compressed'//opcion minificada
      // outputStyle:'compressed',
      sourceComments:true//para que comente el codigo y diga de que liena sass viene deffault false
    }))
    .pipe(autoprefixer({
      versions:['last 2 browsers']
    }))
    .pipe(gulp.dest('../css'));
});

//watch para ver qeu realize los cambio en tiempo real
//ademas es una tarea por defecto que va a ejecutar gulp
gulp.task('default', ()=>{
  gulp.watch('./scss/*.scss', ['sass']);
});