const gulp = require('gulp');
// const uglify = require('gulp-uglify-es');
let uglify = require('gulp-uglify-es').default;

gulp.task('watch', ()=>{
  var watcher = gulp.watch('js/**/*.js');
  watcher.on('change', (event)=>{
    console.log('File '+event.path+' was changed!');
    minificar();
  });
});

gulp.task('uglify', ()=>{
  minificar();
});

function minificar(){
  return gulp.src('js/**/*.js')
    // .pipe(rename("bundle.min.js"))
    // .pipe(sourcemaps.init())
    .pipe(uglify())
    // .pipe(sourcemaps.write()) // Inline source maps.
    // For external source map file:
    //.pipe(sourcemaps.write("./maps")) // In this case: lib/maps/bundle.min.js.map
    .pipe(gulp.dest('../js'));
  console.log('Minificado correctamente');
  // gulp.src('js/**/*.js')
  //   .pipe(uglify())
  //   .pipe(gulp.dest('../js'));
}

gulp.task('default', ()=>{
  console.log('Gulp is runing correctly!');
});