$(document).ready(function(){
		$('#bt_agregar').click(function(){
			agregar();
		});
	});
	var total=0;
	cont=0;
	total=0;
	subtotal=[];
	$("#guardar").hide();
	$("#cancelar").show();
	$("#pidarticulo").change(mostrarValores);

	function mostrarValores()
	{
		datosArticulo=document.getElementById('pidarticulo').value.split('_');
		$("#pprecio_venta").val(datosArticulo[1]);
	}

	function agregar()
	{
		datosArticulo=document.getElementById('pidarticulo').value.split('_');

		idarticulo=datosArticulo[0];
		articulo=$("#pidarticulo option:selected").text();
		cantidad=$("#pcantidad").val(); 
		precio_compra=$("#pprecio_compra").val();
		precio_venta=$("#pprecio_venta").val();

		if (idarticulo!="" && cantidad!="" && cantidad>0 && precio_compra!="" && precio_venta!="")
		{
			subtotal[cont]=(cantidad*precio_compra);
			total=total+subtotal[cont];
			var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-danger" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idarticulo[]"  value="'+idarticulo+'">'+articulo+'</td><td><input type="hidden" name="cantidad[]"  value="'+cantidad+'" readonly="">'+cantidad+'</td><td><input type="hidden" name="precio_compra[]"  value="'+precio_compra+'" readonly="">'+precio_compra+'</td><td><input type="hidden" name="precio_venta[]"  value="'+precio_venta+'" readonly="">'+precio_venta+'</td><td>'+subtotal[cont]+'</td></tr>';
			cont++;
			limpiar();
			  $('#total').html("Bs/ " + total);
			  $('#total_venta').val(total);
			evaluar();
			  $('#detalles').append(fila);			
		}
	  	else
		{
			alert("Revisar la cantidad y el precio de compra");
		}
	 }
	function limpiar()
	{
		$("#pcantidad").val("");
		$("#pprecio_compra").val("");
		$("#pprecio_venta").val("");
	}

	function evaluar()
	{
		if (total>0) 
		{
			$("#guardar").show();
			$("#cancelar").hide();
		}
		else
		{
			$("#guardar").hide();
			$("#cancelar").show();
		}
	}
	function eliminar(index)
	{
		total=total-subtotal[index];
		$('#total').html("Bs/. "+total);
		$('#fila'+index).remove();
		evaluar();
    }
    