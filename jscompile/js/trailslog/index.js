function eliminarSeleccionados() {
    var keys = $('#crud-datatable-pjax').yiiGridView('getSelectedRows');
    $.ajax({
        beforeSend: function (xhr) {
            $("#loading").css("display", "block");
        },
        complete: function (jqXHR, textStatus) {
            $("#loading").css("display", "none");
        },
        success: function (data) {},
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == "403") {
                location.reload();
            }
        },
        type: 'POST',
        dataType: "JSON",
        url: "/trailslog/bulk-delete",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        data: {
            'pks': keys,
        },
    });
}