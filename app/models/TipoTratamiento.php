<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_tratamiento".
 *
 * @property int $id
 * @property string $codigo
 * @property string $nombre
 * @property int $cantidad
 *
 * @property Gasto[] $gastos
 * @property Kardex[] $kardexes
 * @property Reserva[] $reservas
 */
class TipoTratamiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_tratamiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'nombre', 'cantidad'], 'required'],
            [['cantidad'], 'integer'],
            [['codigo', 'nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['tratamiento_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKardexes()
    {
        return $this->hasMany(Kardex::className(), ['tratamiento_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['tratamiento_id' => 'id']);
    }
}
