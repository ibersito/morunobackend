<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kardex;

/**
 * KardexSearch represents the model behind the search form of `app\models\Kardex`.
 */
class KardexSearch extends Kardex
{
    public $fechaInicio;
    public $fechaFin;
    public $fecha;
    public $tipo;
    public $telefono;
    public $tipo_paciente;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'paciente_id', 'tratamiento_id'], 'integer'],
            [['fecha', 'tipo', 'detalle', 'num_doc', 'estado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kardex::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'paciente_id' => $this->paciente_id,
            'fecha' => $this->fecha,
            'tratamiento_id' => $this->tratamiento_id,
        ]);

        $query->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'detalle', $this->detalle])
            ->andFilterWhere(['like', 'num_doc', $this->num_doc])
            ->andFilterWhere(['like', 'estado', $this->estado]);

        return $dataProvider;
    }
    public function ingresos($fechaInicio,$fechaFin,$tipo)
    {
        $query = Kardex::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        $query->joinWith('pagos')
              ->andFilterWhere(['>=', 'fecha', $fechaInicio])
              ->andFilterWhere(['<=', 'fecha', $fechaFin])
              ->andFilterWhere(['=','tipo', $tipo])
              ->groupBy(['{{kardex}}.id'])
              ->all();

        return $dataProvider;
    }
    public function tratamientos($tipo,$fechaInicio,$fechaFin,$tratamiento,$paciente,$telefono,$tipo_paciente)
    {
        $query = Kardex::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false,
        ]);

        $query->joinWith('paciente')
            ->andFilterWhere(['like','kardex.tipo', $tipo])
            ->andFilterWhere(['like','kardex.tratamiento_id', $tratamiento])
            ->andFilterWhere(['like','kardex.paciente_id', $paciente])
            ->andFilterWhere(['like','paciente.telefono', $telefono])
            ->andFilterWhere(['like','paciente.tipo_paciente', $tipo_paciente])
            ->andFilterWhere(['>=', 'kardex.fecha', $fechaInicio])
            ->andFilterWhere(['<=', 'kardex.fecha', $fechaFin])
            ->groupBy(['kardex.id'])
            ->all();

        return $dataProvider;
    }
    public function saldos($fechaInicio,$fechaFin,$tratamiento,$paciente,$telefono)
    {
        $query = Kardex::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false,
        ]);

        $query->joinWith('paciente')->joinWith('pagos')
            ->andFilterWhere(['like','kardex.tratamiento_id', $tratamiento])
            ->andFilterWhere(['like','kardex.paciente_id', $paciente])
            ->andFilterWhere(['like','paciente.telefono', $telefono])
            // ->andFilterWhere(['like', 'estado', 'pendiente'])
            ->andFilterWhere(['>=', 'kardex.fecha', $fechaInicio])
            ->andFilterWhere(['<=', 'kardex.fecha', $fechaFin])
            ->andFilterWhere(['>', 'pagos.monto_total', 0])
            ->groupBy(['kardex.id'])
            ->all();

        return $dataProvider;
    }
}
