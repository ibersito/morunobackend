<?php

namespace app\models;

use app\components\PFunciones;
use Yii;

/**
 * This is the model class for table "pagos".
 *
 * @property int $id
 * @property int $kardex_id
 * @property string $monto_total
 * @property int $cantidad_jeringas
 *
 * @property PagoDetalle[] $pagoDetalles
 * @property Kardex $kardex
 */
class Pagos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pagos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kardex_id', 'monto_total'], 'required'],
            [['kardex_id', 'cantidad_jeringas'], 'integer'],
            [['monto_total'], 'number'],
            [['kardex_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kardex::className(), 'targetAttribute' => ['kardex_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kardex_id' => 'Kardex',
            'monto_total' => 'Monto Total',
            'cantidad_jeringas' => 'Cantidad Jeringas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagodetalles()
    {
        return $this->hasMany(PagoDetalle::className(), ['pago_id' => 'id']);
    }

    public function getPagodetalle()
    {
        return $this->getPagoDetalles()->one();
    }

    public function getJeringas()
    {
        return PFunciones::pageTotal($this->getPagodetalles()->asArray()->all(),'cantidad');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKardex()
    {
        return $this->hasOne(Kardex::className(), ['id' => 'kardex_id']);
    }

    public function getSaldo()
    {
        return $this->monto_total - PFunciones::pageTotal($this->getPagodetalles()->asArray()->all(),'monto_pago');
    }
}
