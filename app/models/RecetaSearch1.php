<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Receta;

/**
 * RecetaSearch represents the model behind the search form of `app\models\Receta`.
 */
class RecetaSearch1 extends Receta
{
    private $pid;
    private $kid;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kardex_id'], 'integer'],
            [['jc', 'fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$kaydexid,$paciente_id)
    {
        $query = Receta::find();
        $this->pid = $paciente_id;
        $this->kid = $kaydexid;
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'kardex_id' => $this->kardex_id,
            'fecha' => $this->fecha,
        ]);
        $query->andWhere(['<>','kardex_id', $this->kid]);
        $query->joinWith(['kardex' => function($query) { $query->andFilterWhere(['paciente_id' => $this->pid]); }]);
        $query->andFilterWhere(['like', 'jc', $this->jc]);

        return $dataProvider;
    }

}