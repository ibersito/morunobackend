<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PagoDetalle;

/**
 * PagoDetalleSearch represents the model behind the search form of `app\models\PagoDetalle`.
 */
class PagoDetalleSearch extends PagoDetalle
{
    public $fechaInicio;
    public $fechaFin;
    public $tipo;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'pago_id', 'cantidad'], 'integer'],
            [['monto_pago'], 'number'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PagoDetalle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pago_id' => $this->pago_id,
            'monto_pago' => $this->monto_pago,
            'cantidad' => $this->cantidad,
            'fecha' => $this->fecha,
        ]);

        return $dataProvider;
    }
    public function todos($fechaInicio, $fechaFin)
    {
        $query = PagoDetalle::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false,
        ]);
        $query->leftjoin('pagos','pago_detalle.pago_id = pagos.id')
        ->leftjoin('kardex','pagos.kardex_id = kardex.id')
        ->andFilterWhere(['>=', 'pago_detalle.fecha', $fechaInicio])
            ->andFilterWhere(['<=', 'pago_detalle.fecha', $fechaFin])
            // ->andFilterWhere(['=', 'kardex.tipo', $tipo])
            ->groupBy(['{{pago_detalle}}.id'])
            ->all();

        return $dataProvider;
    }
    public function ingresos($fechaInicio, $fechaFin, $tipo)
    {
        $query = PagoDetalle::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false,
        ]);
        $query->leftjoin('pagos','pago_detalle.pago_id = pagos.id')
        ->leftjoin('kardex','pagos.kardex_id = kardex.id')
        ->andFilterWhere(['>=', 'pago_detalle.fecha', $fechaInicio])
            ->andFilterWhere(['<=', 'pago_detalle.fecha', $fechaFin])
            ->andFilterWhere(['=', 'kardex.tipo', $tipo])
            ->groupBy(['{{pago_detalle}}.id'])
            ->all();

        return $dataProvider;
    }

  
}