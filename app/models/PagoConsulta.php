<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pago_consulta".
 *
 * @property int $id
 * @property string $monto
 * @property string $fecha
 * @property int $kardex_id
 *
 * @property Kardex $kardex
 */
class PagoConsulta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pago_consulta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['monto', 'fecha', 'kardex_id'], 'required'],
            [['monto'], 'number'],
            [['fecha'], 'safe'],
            [['kardex_id'], 'integer'],
            [['kardex_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kardex::className(), 'targetAttribute' => ['kardex_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'monto' => 'Monto',
            'fecha' => 'Fecha',
            'kardex_id' => 'Kardex ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKardex()
    {
        return $this->hasOne(Kardex::className(), ['id' => 'kardex_id']);
    }
}
