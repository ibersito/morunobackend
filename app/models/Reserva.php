<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $id
 * @property int $paciente_id
 * @property string $fecha
 * @property string $hora
 * @property string $hora_ingreso
 * @property string $hora_llegada
 * @property string $tipo
 * @property int $num_ficha
 * @property int $tratamiento_id
 * @property int $num_ficha
 * @property string $observacion
 *
 * @property TipoTratamiento $tratamiento
 * @property Paciente $paciente
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora', 'tipo', 'hora'], 'required'],
            [['paciente_id', 'tratamiento_id','num_ficha'], 'integer'],
            [['fecha', 'hora', 'hora_ingreso', 'hora_llegada'], 'safe'],
            [['tipo','observacion'], 'string'],
            [['tratamiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoTratamiento::className(), 'targetAttribute' => ['tratamiento_id' => 'id']],
            [['paciente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['paciente_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paciente_id' => 'Paciente',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'hora_ingreso' => 'Hora Ingreso',
            'hora_llegada' => 'Hora Llegada',
            'tipo' => 'Tipo',
            'num_ficha' => 'Número ficha',
            'tratamiento_id' => 'Tratamiento',
            'num_ficha' => 'Número Ficha',
            'observacion' => 'Observación'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTratamiento()
    {
        return $this->hasOne(TipoTratamiento::className(), ['id' => 'tratamiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'paciente_id']);
    }
}
