<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gasto".
 *
 * @property int $id
 * @property string $nombre
 * @property int $monto
 * @property string $tipo_gasto
 * @property int $tratamiento_id
 * @property int $cantidad
 * @property string $fecha
 *
 * @property TipoTratamiento $tratamiento
 */
class Gasto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gasto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'monto', 'tipo_gasto', 'fecha'], 'required'],
            [['monto', 'tratamiento_id', 'cantidad'], 'integer'],
            [['tipo_gasto'], 'string'],
            [['fecha'], 'safe'],
            [['nombre'], 'string', 'max' => 255],
            [['tratamiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoTratamiento::className(), 'targetAttribute' => ['tratamiento_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'monto' => 'Monto',
            'tipo_gasto' => 'Tipo Gasto',
            'tratamiento_id' => 'Tratamiento',
            'cantidad' => 'Cantidad',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTratamiento()
    {
        return $this->hasOne(TipoTratamiento::className(), ['id' => 'tratamiento_id']);
    }
}
