<?php

use yii\helpers\Html;
use kartik\file\FileInput;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\TipoTratamiento;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Gasto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gasto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'monto')->textInput(['type' => 'number', 'min' => '0']) ?>

    <?= $form->field($model, 'tipo_gasto')->widget(Select2::classname(), [
            'data' => [
                'General' => "General",
                'Tratamiento' => "Tratamiento",
            ],
            'options' => ['placeholder' => 'Seleccione una opcion ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>

    <div id="oculto">
        <?= $form->field($model, 'tratamiento_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TipoTratamiento::find()->all(), 'id', 'nombre'),
                'options' => ['placeholder' => 'Seleccione un tratamiento...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        ?>

        <?= $form->field($model, 'cantidad')->textInput(['type' => 'number', 'min' => '0']) ?>
    </div>
    
    <div class="form-group text-right">
        <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['index'], [
            'class' => 'btn btn-default',
            'title' => 'Cerrar',
        ]) ?>
        <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', [
            'class' => 'btn btn-success',
            'aria-label' => 'Pago',
            'data-confirm' => '¿ Esta seguro de crear este elemento ?',
            'data-method' => 'post']) 
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('/js/gasto/_form.js', [
    'depends' => [\yii\web\JqueryAsset::className()],
]); ?>