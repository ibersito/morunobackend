<?php

use app\models\User;
use yii\helpers\Url;
use app\components\PFunciones;
use app\models\Paciente;
use app\models\PagoConsulta;
use app\models\PagoDetalle;

/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>

<div class="site-index">
	<div class="row">
		<div class="col-lg-3 col-md-6 col-sm-6">
			<a href="<?= Url::to(['/admin/user']) ?>">
				<div class="card card-stats">
					<div class="card-header" data-background-color="green">
						<i class="material-icons">face</i>
					</div>
					<div class="card-content">
						<p class="category">Usuarios</p>
						<h3 class="title">Usuarios</h3>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<a href="<?= Url::to(['/paciente']) ?>">
				<div class="card card-stats">
					<div class="card-header" data-background-color="blue">
						<i class="material-icons">person_add_alt_1</i>
					</div>
					<div class="card-content">
						<p class="category">Pacientes</p>
						<h3 class="title">Pacientes</h3>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<a href="<?= Url::to(['/tipotratamiento']) ?>">
				<div class="card card-stats">
					<div class="card-header" data-background-color="red">
						<i class="material-icons">enhanced_encryption</i>
					</div>
					<div class="card-content">
						<p class="category">Tratamientos</p>
						<h3 class="title">Tratamientos</h3>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<a href="<?= Url::to(['/reserva']) ?>">
				<div class="card card-stats">
					<div class="card-header" data-background-color="dark">
						<i class="material-icons">event_available</i>
					</div>
					<div class="card-content">
						<p class="category">Reservas</p>
						<h3 class="title">Reservas</h3>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<a href="<?= Url::to(['/emergencia']) ?>">
				<div class="card card-stats">
					<div class="card-header" data-background-color="pink">
						<i class="material-icons">event_available</i>
					</div>
					<div class="card-content">
						<p class="category">Emergencias</p>
						<h3 class="title">Emergencias</h3>
					</div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<a href="<?= Url::to(['/emergencia']) ?>">
				<div class="card card-stats">
					<div class="card-header" data-background-color="blue">
						<i class="material-icons">event_available</i>
					</div>
					<div class="card-content">
						<p class="category">Gastos</p>
						<h3 class="title">Gastos</h3>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card card-stats">
				<div class="card-header" data-background-color="orange">
					<i class="material-icons">content_copy</i>
				</div>
				<div class="card-content">
					<h3 class="category">Número de Usuarios</h3>
					<h3 style="text-align: right;" class="title">
						<?php
						$count = User::find()->count();
						echo $count;
						?>
					</h3>
				</div>
				<div class="card-footer">
					<div class="stats">
						<a href="<?= Url::to(['/admin/user']) ?>" class="small-box-footer"><i class="material-icons">date_range</i> Lista de usuarios</a>
						<!-- <i class="material-icons">list</i> Listado de usuarios -->
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card card-stats">
				<div class="card-header" data-background-color="green">
					<i class="material-icons">store</i>
				</div>
				<div class="card-content">
					<h3 class="category">Total ingresos de hoy</h3>
					<h3 style="text-align: right;" class="title">
						<?php
						$count = (PFunciones::pageTotal(PagoDetalle::find()->where(['fecha' => date('Y-m-d')])->all(), 'monto_pago') + PFunciones::pageTotal(PagoConsulta::find()->where(['fecha' => date('Y-m-d')])->all(), 'monto'));
						echo \Yii::$app->formatter->asCurrency($count);
						?>
					</h3>
				</div>
				<div class="card-footer">
					<div class="stats">
						<a href="<?= Url::to(['/reporte/ingresos']) ?>" class="small-box-footer"><i class="material-icons">date_range</i> Reporte ingresos</a>
						<!-- <i class="material-icons">date_range</i> Listado de usuarios -->
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<div class="card card-stats">
				<div class="card-header" data-background-color="purple">
					<i class="material-icons">people_alt</i>
				</div>
				<div class="card-content">
					<h3 class="category">Número Pacientes</h3>
					<h3 style="text-align: right;" class="title">
						<?php
						$count = Paciente::find()->count();
						echo $count;
						?>
					</h3>
				</div>
				<div class="card-footer">
					<div class="stats">
						<a href="<?= Url::to(['/paciente']) ?>" class="small-box-footer"><i class="material-icons">date_range</i> Listado Pacientes</a>
						<!-- <i class="material-icons">date_range</i> Listado de usuarios -->
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-6 col-sm-6">
			<div class="card card-stats" style="background: #4C4C4C;">
				<div class="card-header" data-background-color="dark">
					<i class="material-icons">query_builder</i>
				</div>
				<div class="card-content" style="text-align: right;">
					<h4 class="category">Fecha de hoy</h4>
					<h3 class="title" style="color: white;"><?= date('d/m/y') ?></h3>
				</div>
			</div>
		</div>
	</div>
</div>