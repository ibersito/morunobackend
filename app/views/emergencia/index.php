<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmergenciaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tratamiento Ambulatorio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emergencia-index">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left">
                            <h4>
                                <?= Html::encode($this->title) ?>
                            </h4>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right pull-right">
                            <?=  Html::a( "<i class='material-icons'>add</i> Nuevo " .'Tratamiento', ['create', 'tipo' => 'emergencia'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>

                                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    

                                            <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'label' => 'Nombre Paciente',
                                    'attribute' => 'paciente.nombrecompleto',
                                    'width' => '500px'
                                ],
                                [
                                    'label' => 'Edad',
                                    'attribute' => 'paciente.edad'
                                ],
                                [
                                    'label' => 'Telefono',
                                    'attribute' => 'paciente.celular',
                                    'width' => '100px'
                                ],
                                [
                                    'label' => 'Fecha consulta',
                                    'attribute' => 'fecha',
                                    'width' => '100px'

                                ],
                                [
                                    'label' => 'Hora consulta',
                                    'attribute' => 'hora',
                                    'width'=>'100px',

                                ],
                                [
                                    'label' => 'Hora ingreso',
                                    'attribute' => 'hora_ingreso',
                                    'width'=>'100px',

                                ],
                                [
                                    'format' => 'raw',
                                    'contentOptions' => ['style' => 'width: 7%;'],
                                    'attribute' => 'tipo',
                                    'width' => '100px'

                                ],
                                [
                                    'label' => 'Tipo tratamiento',
                                    'attribute' => 'tratamiento.codigo',
                                    'width' => '50px'
                                ],

                                [
                                    'class' => 'kartik\grid\ActionColumn',
                                    'urlCreator' => function($action, $model, $key, $index) { 
                                            return Url::to([$action,'id'=>$key]);
                                    },
                                    'width'=>'200px',
                                    'template'=> '{view} {update} {delete} {kardex}',
                                    'buttons'=>[
                                        'view' => function ($url, $model, $key) {
                                            return Html::a('<i class="material-icons">visibility</i>', $url, [
                                                'class'=>'btn btn-info btn-round btn-just-icon', 
                                                'title'=>'Ver',
                                                'aria-label'=>'Ver',
                                                'data-pjax'=>0
                                            ]);
                                        },
                                        'update' => function ($url, $model, $key) {
                                            return Html::a('<i class="material-icons">edit</i>', $url, [
                                                'class'=>'btn btn-warning btn-round btn-just-icon', 
                                                'title'=>'Modificar',
                                                'aria-label'=>'Modificar',
                                                'data-pjax'=>0
                                            ]);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<i class="material-icons">delete</i>', $url, [
                                                'class'=>'btn btn-danger btn-round btn-just-icon', 
                                                'title'=>'Eliminar',
                                                'aria-label'=>'Eliminar',
                                                'data-confirm'=>'¿ Esta seguro de eliminar este elemento ?',
                                                'data-method'=>'post'
                                            ]);
                                        },
                                        'kardex' => function ($url, $model, $key) {
                                            if ($model->paciente_id == NULL) {
                                                return '';
                                            }
                                            return Html::a(
                                                '<i class="material-icons">content_paste</i>',
                                                ['/paciente/kardex', 'id' => $model->paciente_id],
                                                [
                                                    'class' => 'btn btn-primary btn-just-icon btn-round',
                                                    'title' => 'Kardex',
                                                ]
                                            );
                                        },
                                    ]
                                ],
                            ],
                            'bordered' => true,
                            'striped' => false,
                            'hover' => true,
                            'condensed' => false,
                            'responsive' => true,          
                            'responsiveWrap' => false,                 
                            'resizableColumns' => false,   
                        ]); ?>
                                        
                </div>
            </div>
        </div>
    </div>
</div>