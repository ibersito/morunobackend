<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\TipoTratamiento;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */

$this->title = 'Cambio Ficha Paciente: ' . $paciente->paciente->nombrecompleto;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="paciente-create">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-centered">
            <div class="card">
                <div class="card-content">
                    <h4><?= Html::encode($this->title) ?></h4>
                    <div class="reserva-form">
                        <?php $form = ActiveForm::begin(); ?>
                            <div class="row">
                                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                                    <label>Paciente Actual</label>
                                    <input class="form-control" type="text" disabled value="<?= $paciente->paciente->nombrecompleto ?>">
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                    <label>Número Ficha</label>
                                    <input class="form-control" type="text" disabled value="<?= $paciente->num_ficha ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                                    <?= $form->field($model, 'paciente_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map($pacientes, 'paciente.id',
                                            function ($model) {
                                                return $model->paciente->nombrecompleto .' - Ficha '. $model['num_ficha'].' - '. $model['hora'];
                                            }
                                        ),
                                        'options' => [
                                            'placeholder' => 'Seleccione un paciente...',
                                            'onchange' => '$.post("/reserva/numficha?id=' . '"+$(this).val(),function(data){
                                                $("select#reserva-num_ficha").html(data);
                                            });',
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                    <?= $form->field($model, 'num_ficha')->dropDownList(['prompt' => ''], ['disabled' => 'disabled']) ?>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <a href="<?= Url::to(['/reserva/index']) ?>" class="btn btn-default">
                                    <i class="material-icons">clear</i> Cerrar
                                </a>
                                <button class="btn btn-success" type="submit">
                                    <i class="material-icons">save</i> Cambiar
                                </button>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>