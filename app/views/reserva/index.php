<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reservas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserva-index">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
            <div class="card card-stats">
                <div class="card-header" data-background-color="purple">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left">
                            <h4>
                                <?= Html::encode($this->title) ?>
                            </h4>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-right pull-right">
                            <?= Html::a("<i class='material-icons'>add</i> Nuevo " . 'Reserva', ['create', 'tipo' => 'reserva'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>

                    <?php // echo $this->render('_search', ['model' => $searchModel]); 
                    ?>
                        <h4 class="text-center">Sin Ficha</h4>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'label' => 'Nombre Paciente',
                                'attribute' => 'paciente.nombrecompleto',
                                'width' => '500px',

                            ],
                            [
                                'label' => 'Edad',
                                'attribute' => 'paciente.edad',
                                'width' => '40px',
                            ],
                            [
                                'label' => 'Telefono',
                                'attribute' => 'paciente.celular',
                                'width' => '90px',
                            ],
                            [
                                'label' => 'Fecha consulta',
                                'attribute' => 'fecha',
                                'filterType' => '\kartik\date\DatePicker',
                                'format' => ['date', 'php:d-m-yy'],
                                'width' => '100px',
                                'filterWidgetOptions' => [
                                    'language' => 'es',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ]
                                ],
                            ],
                          
                            [
                                'label' => 'Hora consulta',
                                'attribute' => 'hora',
                                'width' => '80px',

                            ],
                            [
                                'label' => 'Hora ingreso',
                                'attribute' => 'hora_ingreso',
                                'width' => '80px',
                            ],
                            [
                                'label' => 'Hora llegada',
                                'attribute' => 'hora_llegada',
                                'width' => '80px',

                            ],
                            [
                                'label' => 'Ficha',
                                'attribute' => 'num_ficha',
                                'width' => '30px'
                            ],
                            [
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 7%;'],
                                'attribute' => 'tipo',
                            ],
                            [
                                'label' => 'Tipo tratamiento',
                                'attribute' => 'tratamiento.codigo',
                                'width' => '100px',
                            ],

                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to([$action, 'id' => $key]);
                                },
                                'width' => '200px',
                                'template' => '{view} {update} {delete} {kardex} {ficha}',
                                'buttons' => [
                                    'view' => function ($url, $model, $key) {
                                        return Html::a('<i class="material-icons">visibility</i>', $url, [
                                            'class' => 'btn btn-info btn-round btn-just-icon',
                                            'title' => 'Ver',
                                            'aria-label' => 'Ver',
                                            'data-pjax' => 0
                                        ]);
                                    },
                                    'update' => function ($url, $model, $key) {
                                        return Html::a('<i class="material-icons">edit</i>', ['/reserva/update','tipo' => 'reserva','id' => $model->id,'paciente_id' => $model->paciente_id], [
                                            'class' => 'btn btn-warning btn-round btn-just-icon',
                                            'title' => 'Modificar',
                                            'aria-label' => 'Modificar',
                                            'data-pjax' => 0
                                        ]);
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        return Html::a('<i class="material-icons">delete</i>', $url, [
                                            'class' => 'btn btn-danger btn-round btn-just-icon',
                                            'title' => 'Eliminar',
                                            'aria-label' => 'Eliminar',
                                            'data-confirm' => '¿ Esta seguro de eliminar este elemento ?',
                                            'data-method' => 'post'
                                        ]);
                                    },
                                    'kardex' => function ($url, $model, $key) {
                                        if ($model->paciente_id == NULL) {
                                            return '';
                                        }
                                        return Html::a(
                                            '<i class="material-icons">content_paste</i>',
                                            ['/paciente/kardex', 'id' => $model->paciente_id,'tipo' => 'reserva'],
                                            [
                                                'class' => 'btn btn-primary btn-just-icon btn-round',
                                                'title' => 'Kardex',
                                            ]
                                        );
                                    },
                                    'ficha' => function ($url, $model, $key) {
                                        if($model->paciente_id == NULL){
                                            return '';
                                        }
                                        return Html::a(
                                            '<i class="material-icons">account_box</i>',
                                            ['/reserva/ficha', 'id' => $model->id],
                                            [
                                                'class' => 'btn btn-info btn-just-icon btn-round',
                                                'title' => 'Asignar Ficha',
                                            ]
                                        );
                                    },
                                ]
                            ],
                        ],
                        'bordered' => true,
                        'striped' => false,
                        'hover' => true,
                        'condensed' => false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'resizableColumns' => false,
                    ]); ?>
                    
                    <br>
                    <br>
                    <br>
                    <br>
                        <h4 class="text-center">Con Ficha</h4>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider2,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'label' => 'Nombre Paciente',
                                'attribute' => 'paciente.nombrecompleto',
                                'width' => '500px',

                            ],
                            [
                                'label' => 'Edad',
                                'width' => '40px',
                                'attribute' => 'paciente.edad'
                            ],
                            [
                                'label' => 'Telefono',
                                'width' => '90px',
                                'attribute' => 'paciente.celular'
                            ],
                            [
                                'label' => 'Fecha consulta',
                                'attribute' => 'fecha',
                                'filterType' => '\kartik\date\DatePicker',
                                'format' => ['date', 'php:d-m-yy'],
                                'width' => '100px',
                                'filterWidgetOptions' => [
                                    'language' => 'es',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ]
                                ],
                            ],
                            [
                                'label' => 'Hora consulta',
                                'attribute' => 'hora',
                                'width' => '80px',
                            ],
                            [
                                'label' => 'Hora ingreso',
                                'attribute' => 'hora_ingreso',
                                'width' => '80px',
                            ],
                            [
                                'label' => 'Hora llegada',
                                'attribute' => 'hora_llegada',
                                'width' => '80px',
                            ],
                            [
                                'label' => 'Ficha',
                                'attribute' => 'num_ficha',
                                'width' => '30px',

                            ],
                            [
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 7%;'],
                                'attribute' => 'tipo',
                                'width' => '100px',

                            ],
                            [
                                'label' => 'Tipo tratamiento',
                                'attribute' => 'tratamiento.codigo',
                                'width' => '100px',

                            ],

                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    return Url::to([$action, 'id' => $key]);
                                },
                                'width' => '200px',
                                'template' => '{view} {update} {delete} {kardex} {ficha}',
                                'buttons' => [
                                    'view' => function ($url, $model, $key) {
                                        return Html::a('<i class="material-icons">visibility</i>', $url, [
                                            'class' => 'btn btn-info btn-round btn-just-icon',
                                            'title' => 'Ver',
                                            'aria-label' => 'Ver',
                                            'data-pjax' => 0
                                        ]);
                                    },
                                    'update' => function ($url, $model, $key) {
                                        return Html::a('<i class="material-icons">edit</i>', ['/reserva/update','tipo' => 'reserva','id' => $model->id,'paciente_id' => $model->paciente_id], [
                                            'class' => 'btn btn-warning btn-round btn-just-icon',
                                            'title' => 'Modificar',
                                            'aria-label' => 'Modificar',
                                            'data-pjax' => 0
                                        ]);
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        return Html::a('<i class="material-icons">delete</i>', $url, [
                                            'class' => 'btn btn-danger btn-round btn-just-icon',
                                            'title' => 'Eliminar',
                                            'aria-label' => 'Eliminar',
                                            'data-confirm' => '¿ Esta seguro de eliminar este elemento ?',
                                            'data-method' => 'post'
                                        ]);
                                    },
                                    'kardex' => function ($url, $model, $key) {
                                        if ($model->paciente_id == NULL) {
                                            return '';
                                        }
                                        return Html::a(
                                            '<i class="material-icons">content_paste</i>',
                                            ['/paciente/kardex', 'id' => $model->paciente_id,'tipo' => 'reserva'],
                                            [
                                                'class' => 'btn btn-primary btn-just-icon btn-round',
                                                'title' => 'Kardex',
                                            ]
                                        );
                                    },
                                    'ficha' => function ($url, $model, $key) {
                                        if($model->paciente_id == NULL){
                                            return '';
                                        }
                                        return Html::a(
                                            '<i class="material-icons">account_box</i>',
                                            ['/reserva/ficharemove', 'id' => $model->id],
                                            [
                                                'class' => 'btn btn-danger btn-just-icon btn-round',
                                                'title' => 'Eliminar Ficha',
                                            ]
                                        );
                                    },
                                ]
                            ],
                        ],
                        'bordered' => true,
                        'striped' => false,
                        'hover' => true,
                        'condensed' => false,
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'resizableColumns' => false,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>