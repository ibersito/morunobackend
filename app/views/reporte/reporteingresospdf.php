<?php

    use app\components\PFunciones;

?>
<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de ingresos</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>Cantidad jeringas</th>
                        <th>Tratamiento</th>
                        <th>Tipo</th>
                        <th>Monto</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($ingresos->models as $ingreso){ ?>
                        <tr>
                            <td><?= $ingreso->pago->kardex->paciente->nombrecompleto ?></td>
                            <td><?= $ingreso['cantidad'] ?></td>
                            <td><?= $ingreso->pago->kardex->tratamiento ? $ingreso->pago->kardex->tratamiento->nombre : '(ND)' ?></td>
                            <td><?= $ingreso->pago->kardex->tipo ?></td>
                            <td><?= $ingreso['monto_pago'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"><b>TOTALES</b></td>
                        <td><?= Yii::$app->formatter->asCurrency(PFunciones::pageTotal($ingresos->models, 'monto_pago')) ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>detalle</th>
                        <th>fecha</th>
                        <th>Tipo</th>
                        <th>Monto</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($ingresosConsulta->models as $ingresoConsul){ ?>
                        <tr>
                            <td><?= $ingresoConsul->kardex->paciente->nombrecompleto ?></td>
                            <td><?= $ingresoConsul->kardex->detalle ?></td>
                            <td><?= $ingresoConsul->kardex->fecha ?></td>
                            <td><?= $ingresoConsul->kardex->tipo ?></td>
                            <td><?= $ingresoConsul['monto'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"><b>TOTALES</b></td>
                        <td><?= Yii::$app->formatter->asCurrency(PFunciones::pageTotal($ingresosConsulta->models, 'monto')) ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
   
</div>