<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\components\PTotal;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\TipoTratamiento;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reporte de egresos';
$this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="plan-index">
    <div class="card">
        <div class="card-content">
            <div class="row" style="background-color:#f5f5f5; border-radius:15px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php $form = ActiveForm::begin([
                        // 'type' => ActiveForm::TYPE_HORIZONTAL,
                        'options' => [
                            'class' => 'disable-submit-buttons',
                        ]
                    ]); ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <?= $form->field($model, 'fechaInicio')->hint('Año - Mes - Dia')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Seleccione la fecha', 'autocomplete' => 'off'],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <?= $form->field($model, 'fechaFin')->hint('Año - Mes - Dia')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Seleccione la fecha', 'autocomplete' => 'off'],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <?= $form->field($model, 'tipo_gasto')->widget(Select2::classname(), [
                                'data' => [
                                    'General' => "General",
                                    'Tratamiento' => "Tratamiento",
                                ],
                                'options' => ['placeholder' => 'Seleccione una opcion ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <?= $form->field($model, 'tratamiento_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(TipoTratamiento::find()->all(), 'id', 'nombre'),
                                'options' => ['placeholder' => 'Seleccione un tratamiento...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                            <div class="form-group" style="padding-top:15px;">
                                <?= Html::submitButton('<i class="material-icons">search</i> Buscar', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                            <div class="form-group" style="padding-top: 15px;padding-left:10px">
                                <?= Html::a('<i class="material-icons">autorenew</i> Limpiar', ['reporte/egresos'], ['class' => 'btn btn-warning btn-round']) ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                            <div class="form-group" style="padding-top: 15px;padding-left:10px">
                                <?= Html::a('<i class="material-icons">print</i> Imprimir', ['reporteegresospdf', 'fechaInicio' => $fechaInicio, 'fechaFin' => $fechaFin, 'tipo_gasto' => $tipo_gasto, 'tratamiento' => $tratamiento], ['class' => 'btn btn-info btn-round', 'target' => '_blank']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <?= GridView::widget([
                'dataProvider' => $egresos,
                'showFooter' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Nombre',
                        'attribute' => 'nombre'
                    ],
                    [
                        'label' => 'Monto',
                        'attribute' => 'monto',
                    ],
                    [
                        'label' => 'Tipo Gasto',
                        'attribute' => 'tipo_gasto',
                    ],
                    [
                        'label' => 'Tratamiento',
                        'attribute' => 'tratamiento.nombre'
                    ],
                    [
                        'label' => 'Cantidad',
                        'attribute' => 'cantidad'
                    ],
                    [
                        'label' => 'Fecha',
                        'attribute' => 'fecha'
                    ],
                ],
                'bordered' => true,
                'striped' => false,
                'hover' => true,
                'condensed' => false,
                'responsive' => true,
                'responsiveWrap' => false,
                'resizableColumns' => false,
            ]); ?>

            <!-- <div class="form-group text-right">
                <?= Html::a('<i class="material-icons">clear</i> Cerrar', ['index'], ['class' => 'btn btn-round btn-default']) ?>
            </div> -->
        </div>
    </div>
</div>