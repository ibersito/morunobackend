<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de saldos</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>Tratamiento</th>
                        <th>Cantidad jeringas</th>
                        <th>Monto total</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($saldos->models as $saldo){ ?>
                        <tr>
                            <td><?= $saldo->paciente->nombrecompleto ?></td>
                            <td><?= $saldo->tratamiento->nombre ?></td>
                            <td><?= $saldo->pago->jeringas?></td>
                            <td><?= $saldo->pago->monto_total?></td>
                            <td><?= $saldo->pago->saldo ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>