<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de egresos</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Monto</th>
                        <th>Tipo gasto</th>
                        <th>Tratamiento</th>
                        <th>Cantidad</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($egresos->models as $egreso) { ?>
                        <tr>
                            <td><?= $egreso['nombre'] ?></td>
                            <td><?= $egreso['monto'] ?></td>
                            <td><?= $egreso['tipo_gasto'] ?></td>
                            <td><?= $egreso->tratamiento->nombre  ?></td>
                            <td><?= $egreso['cantidad'] ?></td>
                            <td><?= $egreso['fecha'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>