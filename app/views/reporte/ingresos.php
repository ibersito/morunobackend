<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\components\PFunciones;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reporte de ingresos';
$this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="plan-index">
    <div class="card">
        <div class="card-content">
            <div class="row" style="background-color:#f5f5f5; border-radius:15px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php $form = ActiveForm::begin([
                        // 'type' => ActiveForm::TYPE_HORIZONTAL,
                        'options' => [
                            'class' => 'disable-submit-buttons',
                        ]
                    ]); ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <?= $form->field($model, 'fechaInicio')->hint('Año - Mes - Dia')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Seleccione la fecha', 'autocomplete' => 'off'],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <?= $form->field($model, 'fechaFin')->hint('Año - Mes - Dia')->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => 'Seleccione la fecha', 'autocomplete' => 'off'],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                            <?= $form->field($model, 'tipo')->widget(Select2::classname(), [
                                'data' => [
                                    'Todo' => "Todo",
                                    'Consulta' => "Consulta",
                                    'Reconsulta' => "Reconsulta",
                                    'Tratamiento' => "Tratamiento",
                                ],
                                'options' => ['placeholder' => 'Seleccione una opcion ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                            <div class="form-group" style="padding-top:15px;">
                                <?= Html::submitButton('<i class="material-icons">search</i> Buscar', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                            <div class="form-group" style="padding-top:15px;padding-left:10px;">
                                <?= Html::a('<i class="material-icons">autorenew</i> Limpiar', ['reporte/ingresos'], ['class' => 'btn btn-warning']) ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                            <div class="form-group" style="padding-top: 15px;padding-left:15px;">
                                <?= Html::a('<i class="material-icons">print</i> Imprimir', ['reporteingresospdf', 'fechaInicio' => $fechaInicio, 'fechaFin' => $fechaFin, 'tipo' => $tipo], ['class' => 'btn btn-info btn-round', 'target' => '_blank']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <?= GridView::widget([
                'dataProvider' => $ingresos,
                'showFooter' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Paciente',
                        'attribute' => 'pago.kardex.paciente.nombrecompleto'
                    ],
                    [
                        'label' => 'Detalle',
                        'attribute' => 'pago.kardex.detalle'
                    ],
                    [
                        'label' => 'Cantidad jeringas',
                        'attribute' => 'cantidad'
                    ],
                    [
                        'label' => 'Tratamiento',
                        'attribute' => 'pago.kardex.tratamiento.nombre',
                    ],
                    [
                        'label' => 'Tipo',
                        'attribute' => 'pago.kardex.tipo',
                        'footer' => '<b>TOTALES</b>'
                    ],
                    [
                        'label' => 'Monto',
                        'attribute' => 'monto_pago',
                        'footer' => Yii::$app->formatter->asCurrency(PFunciones::pageTotal($ingresos->models, 'monto_pago'))
                    ],
                ],
                'bordered' => true,
                'striped' => false,
                'hover' => true,
                'condensed' => false,
                'responsive' => true,
                'responsiveWrap' => false,
                'resizableColumns' => false,
            ]); ?>
            <br>
            <br>
            <?= GridView::widget([
                'dataProvider' => $ingresosConsulta,
                'showFooter' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Paciente',
                        'attribute' => 'kardex.paciente.nombrecompleto'
                    ],
                    [
                        'label' => 'Detalle',
                        'attribute' => 'kardex.detalle'
                    ],
                    [
                        'label' => 'Fecha',
                        'attribute' => 'fecha',
                    ],
                    [
                        'label' => 'Tipo',
                        'attribute' => 'kardex.tipo',
                        'footer' => '<b>TOTALES</b>'
                    ],
                    [
                        'label' => 'Monto',
                        'attribute' => 'monto',
                        'footer' => Yii::$app->formatter->asCurrency(PFunciones::pageTotal($ingresosConsulta->models, 'monto'))
                    ],
                ],
                'bordered' => true,
                'striped' => false,
                'hover' => true,
                'condensed' => false,
                'responsive' => true,
                'responsiveWrap' => false,
                'resizableColumns' => false,
            ]); ?>

            <!-- <div class="form-group text-right">
                <?= Html::a('<i class="material-icons">clear</i> Cerrar', ['index'], ['class' => 'btn btn-round btn-default']) ?>
            </div> -->
        </div>
    </div>
</div>