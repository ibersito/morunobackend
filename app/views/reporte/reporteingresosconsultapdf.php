<?php

    use app\components\PFunciones;

?>
<div class="size-middle-carta visible-print-block text-uppercase">
    <div class="text-center">
        <h3>Reporte de ingresos consultas</h3>
    </div>
    <div class="card">
        <div class="card-content">
            <table class="table table-bordered text-uppercase">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Monto</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($ingresos->models as $ingreso){ ?>
                        <tr>
                            <td><?= $ingreso->kardex->paciente->nombrecompleto ?></td>
                            <td><?= $ingreso['fecha'] ?></td>
                            <td><?= $ingreso->kardex->tipo ?></td>
                            <td><?= $ingreso['monto'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"><b>TOTALES</b></td>
                        <td><?= Yii::$app->formatter->asCurrency(PFunciones::pageTotal($ingresos->models, 'monto')) ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>