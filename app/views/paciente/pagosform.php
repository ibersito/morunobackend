<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pagos */

$this->title = 'Nuevo Pago';
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex','id'=>$kardex->paciente_id]];
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['pagos','id'=>$kardex->paciente_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receta-create">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-centered">
            <div class="card">
                <div class="card-content">
                    <h4><?= Html::encode($this->title) ?></h4>
                    <div class="paciente-form">
                        <?php $form = ActiveForm::begin(); 
                        ?>
                            <?= $form->field($model, 'monto_total')->textInput(['type' => 'number','min' => '0','maxlength' => true]) ?>

                            <?= $form->field($model, 'cantidad_jeringas')->textInput(['type' => 'number','min' => '0','maxlength' => true]) ?>

                            <div class="form-group text-right">
                                <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['paciente/pagos','id'=>$id], [
                                    'class' => 'btn btn-default',
                                    'title' => 'Cerrar',
                                ]) ?>
                                <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>