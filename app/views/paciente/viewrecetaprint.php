<div class="container objeto-oculto">
    <div class="col-md-2">
   <a class="btn btn-primary btn-lg" id="btnImprimir">
    <i class="fa fa-fw fa-print"></i> Imprimir receta
  </a>
</div>
</div>


<div id="size-receta" class="text-uppercase">
    <div class="paciente">
        <p class="recetanombre">
            <strong>
                <?= $receta->kardex->paciente->nombrecompleto ?>
            </strong>
        </p>
        <p class="recetajc">
            <strong>
                JC: <?= $receta->jc ?>
            </strong>
        </p>
    </div>
    <div class="receta noborder">
        <table class="table table-striped table-condensed table-hover">
            <tbody>
                <?php for ($i = 0; $i < count($detalles); $i++) { ?>
                    <tr>
                        <td><?= $i + 1 ?>.</td>
                        <td width="95%"><?= $detalles[$i]['detalle'] ?></td>
                        <td width="20%"># <?= $detalles[$i]['valores'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="fecha">
        <div class="text-center text-uppercase receta-dia">
            <?= Yii::$app->formatter->asDate($receta->fecha, 'php:d') ?>
        </div>
        <div class="text-center text-uppercase receta-mes" >
            <?= Yii::$app->formatter->asDate($receta->fecha, 'php:F') ?>
        </div>
        <div class="text-center text-uppercase receta-año" >
            <?= Yii::$app->formatter->asDate($receta->fecha, 'yy') ?>
        </div>
    </div>
</div>




<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
     function imprimir(){
    
    $(".objeto-oculto").css("display","none");
    $(".bloque-de-impresion").css("display","block");
    window.print();
    location.reload();
}

$(document).on("click","#btnImprimir",function(e){
    imprimir();
});
</script>

<style type="text/css">
    /**
* reporte detalle receta paciente
* ancho 15.8 cm
* alto 20.3 cm
*/

@font-face {
  font-family: "pepsi";
  src: url(../fonts/pepsi-light.ttf);
  
}
#size-receta {
    margin: 0;
    padding-top: 2px;
    /* background-color: black; */
}

.noborder table tbody tr td {
    border: none;
    
}

.receta table {
 
    font-size: 14px;
    /* width: 80%; */
    margin-left: 35px;
    margin-right: 35px;
    position: absolute;
}

.paciente {
    padding-top: 55px;
}

.recetanombre {
    padding-left: 90px;
    /*padding-top: 2px;*/
    font-size: 15px
}

.recetajc {
    padding-left: 30px;
    padding-top: 5px;
    font-size: 15px;
}

.fecha {
    
    position: absolute;
    padding-top: 425px;
    padding-left: 150px;
    font-size: 14px;
    width: 452px;

}

.receta-dia {
    /*padding-left: 160px;*/
    float: left;
    width: 10.3%;
}

.receta-mes {
    /*padding-left: -55px;*/
    float: left;
    width: 33.3%;
    padding-left: 50px;
}

.receta-año {
   /* padding-right: -275px;*/
    float: right;
    width: 0.3%;
}


/* @font-face {
    font-family: pepsi-light;
    src: url(../fonts/pepsi-light.woff);
} */

#size-receta {
    font-family: pepsi;
}
</style>