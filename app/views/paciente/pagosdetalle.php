<?php

use app\components\PFunciones;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pagodetalle */

$this->title = 'Detalle Pago';
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex', 'id' => $pagos->kardex->paciente_id]];
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['pagos', 'id' => $pagos->kardex->paciente_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receta-create">
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'monto_pago')->textInput(['type' => 'number', 'min' => '0', 'maxlength' => true]) ?>

                    <?= $form->field($model, 'cantidad')->textInput(['type' => 'number', 'min' => '0', 'maxlength' => true]) ?>

                    <?= $form->field($model, 'fecha')->widget(
                        DatePicker::className(),
                        [
                            'name' => 'check_issue_date',
                            'value' => date('y-m-d', strtotime('+2 days')),
                            'options' => ['placeholder' => 'Seleccione la fecha ...'],
                            'pluginOptions' => [
                                'startDate' => date('Y-m-d'),
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true,
                                'todayHighlight' => true
                            ]
                        ]
                    );
                    ?>

                    <div class="form-group text-right">
                        <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['paciente/pagos', 'id' => $pagos->kardex->id], [
                            'class' => 'btn btn-default',
                            'title' => 'Cerrar',
                        ]) ?>
                        <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', [
                            'class' => 'btn btn-success',
                            'aria-label' => 'Pago',
                            'data-confirm' => '¿ Esta seguro de realizar el pago ?',
                            'data-method' => 'post']) 
                        ?>
                       
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4>Paciente: <?= $pagos->kardex->paciente->nombrecompleto ?> </h4>
                                <h4>Detalle Pago: <?= $pagos->kardex->tratamiento ? $pagos->kardex->tratamiento->nombre : $pagos->kardex->tipo  ?></h4><br>
                                <h4 class="text-left">Jeringas Restantes: <?= $pagos->cantidad_jeringas - PFunciones::pageTotal($detalles,'cantidad')?></h4>
                                <h4 class="text-left">Monto Restante: <?= Yii::$app->formatter->asCurrency($pagos->monto_total - PFunciones::pageTotal($detalles, 'monto_pago')) ?></h4>
                                <h4 class="text-right">Cantidad Jeringas: <?= $pagos ? $pagos->cantidad_jeringas : ''  ?></h4>
                                <h4 class="text-right">Monto Total: <?= Yii::$app->formatter->asCurrency($pagos->monto_total) ?></h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Cantidad</th>
                                            <th>Monto Pagado</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td><b>Totales:</b></td>
                                            <td><b><?= PFunciones::pageTotal($detalles,'cantidad') ?></b></td>
                                            <td><b><?= Yii::$app->formatter->asCurrency(PFunciones::pageTotal($detalles,'monto_pago')) ?></b></td>
                                        </tr>
                                    </tfoot>
                                    <?php foreach($detalles as $detalle) { ?>
                                        <tbody>
                                            <tr>
                                                <td><?= $detalle->fecha ?></td>
                                                <td><?= $detalle->cantidad ?></td>
                                                <td><?= $detalle->monto_pago ?></td>
                                            </tr>
                                        </tbody>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>