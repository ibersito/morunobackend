<div id="size-receta" class="text-uppercase">
    <div class="paciente">
        <p class="recetanombre">
            <strong>
                <?= $receta->kardex->paciente->nombrecompleto ?>
            </strong>
        </p>
        <p class="recetajc">
            <strong>
                JC: <?= $receta->jc ?>
            </strong>
        </p>
    </div>
    <div class="receta noborder">
        <table class="table table-striped table-condensed table-hover">
            <tbody>
                <?php for ($i = 0; $i < count($detalles); $i++) { ?>
                    <tr>
                        <td><?= $i + 1 ?>.</td>
                        <td width="95%"><?= $detalles[$i]['detalle'] ?></td>
                        <td width="20%"># <?= $detalles[$i]['valores'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="fecha">
        <div class="text-center text-uppercase receta-dia">
            <?= Yii::$app->formatter->asDate($receta->fecha, 'php:d') ?>
        </div>
        <div class="text-center text-uppercase receta-mes">
            <?= Yii::$app->formatter->asDate($receta->fecha, 'php:F') ?>
        </div>
        <div class="text-center text-uppercase receta-año">
            <?= Yii::$app->formatter->asDate($receta->fecha, 'yy') ?>
        </div>
    </div>
</div>