<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Receta */

$this->title = 'Detalle Receta';
// $this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex', 'id' => $receta->kardex->paciente_id]];
// $this->params['breadcrumbs'][] = ['label' => 'Receta', 'url' => ['receta', 'id' => $receta->kardex->id]];

if (isset($newkardex)) {
    $this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex', 'id' => $newkardex->paciente_id]];
    $this->params['breadcrumbs'][] = ['label' => 'Receta', 'url' => ['receta', 'id' => $newkardex->id]];
}else{
    $this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => 'Kardex', 'url' => ['kardex', 'id' => $receta->kardex->paciente_id]];
    $this->params['breadcrumbs'][] = ['label' => 'Receta', 'url' => ['receta', 'id' => $receta->kardex->id]];
}


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receta-create">
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'detalle')->textArea(['rows' => '6']) ?>

                        <?= $form->field($model, 'valores')->textInput(['maxlength' => true]) ?>

                        <div class="form-group text-right">
                            
                            <?= Html::a("<i class='material-icons'>clear</i> " . 'Cerrar', ['paciente/receta', 'id' => isset($newkardex) ? $newkardex->id : $receta->kardex->id], [
                                'class' => 'btn btn-default',
                                'title' => 'Cerrar',
                            ]) ?>
                            <?= Html::submitButton("<i class='material-icons'>save</i> " . 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <div class="row">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Detalle</th>
                                    <th>Valores</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <?php for ($i = 0; $i < count($detalles); $i++) { ?>
                                <tbody>
                                    <tr>
                                        <td><?= $detalles[$i]['detalle'] ?></td>
                                        <td><?= $detalles[$i]['valores'] ?></td>
                                        <td class="text-center" width="20%">
                                            <a class="btn btn-warning btn-just-icon btn-round" href="<?= Url::to(['paciente/recetadetalleupdate', 'id' => $detalles[$i]['id']]) ?>" title="Modificar">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a class="btn btn-danger btn-just-icon btn-round" 
                                               href="<?= Url::to(['paciente/recetadetalledelete', 'id' => $detalles[$i]['id']]) ?>" 
                                               title="Eliminar"
                                               aria-label="Eliminar"
                                               data-confirm="¿ Esta seguro de eliminar este elemento ?"
                                               data-method="post"
                                            >
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>