<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=medico',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

 'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,//o 3600
    'schemaCache' => 'cache',
];