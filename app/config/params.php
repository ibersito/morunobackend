<?php


Yii::setAlias('@clienteImgPath', dirname(__DIR__).'/../uploads/clientes/');
Yii::setAlias('@clienteImgUrl', '/uploads/clientes');

Yii::setAlias('@vendedorImgPath', dirname(__DIR__).'/../uploads/vendedores/');
Yii::setAlias('@vendedorImgUrl', '/uploads/vendedores');

Yii::setAlias('@categoriaImgPath', dirname(__DIR__) . '/../uploads/categorias/');
Yii::setAlias('@categoriaImgUrl', '/uploads/categorias');

Yii::setAlias('@productoImgPath', dirname(__DIR__) . '/../uploads/productos/');
Yii::setAlias('@productoImgUrl', '/uploads/productos');

Yii::setAlias('@sliderImgPath', dirname(__DIR__) . '/../uploads/sliders/');
Yii::setAlias('@sliderImgUrl', '/uploads/sliders');

Yii::setAlias('@noticiaImgPath', dirname(__DIR__) . '/../uploads/noticias/');
Yii::setAlias('@noticiaImgUrl', '/uploads/noticias');

Yii::setAlias('@nosotrosImgPath', dirname(__DIR__) . '/../uploads/nosotros/');
Yii::setAlias('@nosotrosImgUrl', '/uploads/nosotros');

Yii::setAlias('@PathApi', 'http://backenddistribuidora.test/api/uploads');

return [
    'moneda'=>' Bs.',
    'label_moneda'=>' Bolivianos',
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'soporte@soporte.com',
    'user.passwordResetTokenExpire' => 3600,
    'GOOGLE_API_KEY' => 'AIzaSyBUf2cDjX6QTyLvUYGe5IQs748Sn_UzBKs',
    'map_zoom_one' => '' 
];
