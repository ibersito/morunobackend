<?php

namespace app\controllers;

use Yii;
use app\models\Gasto;
use yii\web\Controller;
use app\models\Paciente;
use app\models\GastoSearch;
use yii\filters\VerbFilter;
use app\components\PFunciones;
use app\models\TipoTratamiento;
use yii\web\NotFoundHttpException;

/**
 * GastoController implements the CRUD actions for Gasto model.
 */
class GastoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gasto models.
     * @return mixed
     */
    public function actionIndex()
    {
        PFunciones::edad(Paciente::find()->all());

        $searchModel = new GastoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gasto model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gasto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gasto();

        if ($model->load(Yii::$app->request->post())) {
            $tipo_gasto = Yii::$app->request->post('Gasto')['tipo_gasto'];
            if ($tipo_gasto == 'Tratamiento') {
                //modificadno la cantidad del tratamiento
                $tratamiento = TipoTratamiento::findOne($model->tratamiento_id);
                $tratamiento->cantidad = intval($tratamiento->cantidad) + intval($model->cantidad);
                $model->fecha = date('Y-m-d');
                $tratamiento->save();
            }
            $model->fecha = date('Y-m-d');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pago model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //////verificacion si ya esta asignado el paciente a un kardex y una reserva
        $kardex = Gasto::find()->where(['id' => $id])->one();
        // if ($kardex) {
        //     Yii::$app->session->setFlash('error', 'No se pudo eliminar, el paciente ya esta asiganda a una reserva y/o kardex');
        //     return $this->redirect(['index']);
        // }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    

    /**
     * Finds the Gasto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gasto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gasto::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}