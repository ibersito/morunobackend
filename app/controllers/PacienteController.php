<?php

namespace app\controllers;

use Yii;
use kartik\mpdf\Pdf;
use app\models\Pagos;
use app\models\Kardex;
use app\models\Receta;
use app\models\Reserva;
use yii\web\Controller;
use app\models\Paciente;
use app\models\PagoDetalle;
use app\models\PagosSearch;
use yii\filters\VerbFilter;
use app\models\PagoConsulta;
use app\models\RecetaSearch;
use app\models\RecetaSearch1;
use app\models\DetalleReceta;
use app\components\PFunciones;
use app\models\PacienteSearch;
use app\models\TipoTratamiento;
use app\models\PagoConsultaSearch;
use yii\web\NotFoundHttpException;

/**
 * PacienteController implements the CRUD actions for Paciente model.
 */
class PacienteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Paciente models.
     * @return mixed
     */
    public function actionIndex()
    {
        PFunciones::edad(Paciente::find()->all());

        $searchModel = new PacienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Paciente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Paciente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Paciente();
        $tipo = Yii::$app->request->get('tipo');
        $id = Yii::$app->request->get('id');
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {
            $exist = Paciente::find()->where(['nombres' => $request->post('Paciente')['nombres'], 'apellidos' => $request->post('Paciente')['apellidos']])->exists();
            if ($exist) {
                throw new \Exception('El paciente ya se registro en el sistema');
            }
            $model->edad = date("Y") - date("Y", strtotime($request->post('Paciente')['fecha_nacimiento']));
            $model->save();
            $tipo = $request->get('tipo');
            $id = Yii::$app->request->get('id');
            if ($tipo && !$id) {
                return $this->redirect([$tipo . '/create', 'tipo' => $tipo, 'paciente_id' => $model->id]);
            } elseif ($tipo && $id) {
                return $this->redirect([$tipo . '/update', 'tipo' => $tipo, 'paciente_id' => $model->id, 'id' => $id]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'tipo' => $tipo,
            'id' => $id,
        ]);
    }

    /**
     * Updates an existing Paciente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tipo = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'tipo' => $tipo,
        ]);
    }

    /**
     * Deletes an existing Paciente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //////verificacion si ya esta asignado el paciente a un kardex y una reserva
        $kardex = Kardex::find()->where(['paciente_id' => $id])->one();
        if ($kardex) {
            Yii::$app->session->setFlash('error', 'No se pudo eliminar, el paciente ya esta asiganda a una reserva y/o kardex');
            return $this->redirect(['index']);
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionKardex($id)
    {
        $paciente = $this->findModel($id);
        $model = new Kardex();
        $kardexes = kardex::find()->where(['paciente_id' => $id])->all();
        // print_r($kardexes[2]->getPagostoal());
        // echo "aaaa";
        // print_r($kardexes);
        $request = Yii::$app->request;
        $model->fecha = date('Y-m-d');
        $tipo = Yii::$app->request->get('tipo');

        if ($model->load($request->post())) {
            // $maxficha = Reserva::find()->where(['fecha' => $request->post('Kardex')['fecha']])->max('num_ficha');

            ///tipo = paciente(crear reserva - item kardex)
            if ($tipo == 'paciente') {
                if (Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) {
                    //registro del kardex
                    $reserva = new Reserva;
                    $reserva->fecha = date('d-m-Y');
                    $reserva->paciente_id = $paciente->id;
                    // $reserva->fecha = $request->post('Kardex')['fecha'];
                    $reserva->hora = date("h:i:s");
                    $reserva->hora_ingreso = date("h:i:s");
                    $reserva->tipo = $request->post('Kardex')['tipo'];
                    $reserva->num_ficha = 0;
                    $reserva->tratamiento_id = $request->post('Kardex')['tratamiento_id'];
                    $reserva->save();
                    $model->paciente_id = $id;
                    $model->estado = 'Activo';
                    $model->save();
                    // return $this->redirect(['kardex', 'id' => $model->paciente_id]);
                    $this->redirect(Yii::$app->request->referrer);
                }
                if (Yii::$app->user->can('Doctor')) {
                    //creacion de la reserva
                    $reserva = new Reserva;
                    $reserva->fecha = date('d-m-Y');

                    $reserva->paciente_id = $paciente->id;
                    // $reserva->fecha = $request->post('Kardex')['fecha'];
                    $reserva->hora = date("h:i:s");
                    $reserva->hora_ingreso = date("h:i:s");
                    $reserva->tipo = $request->post('Kardex')['tipo'];
                    $reserva->num_ficha = 0;
                    $reserva->tratamiento_id = $request->post('Kardex')['tratamiento_id'];
                    $reserva->save();

                    //registro del kardex
                    $model->paciente_id = $id;
                    $model->estado = 'Pendiente';
                    $model->save();
                    // return $this->redirect(['kardex', 'id' => $model->paciente_id]);
                    $this->redirect(Yii::$app->request->referrer);
                }
            } else {
                ///tipo = reserva (crear item kardex)
                // if($tipo == 'reserva'){
                if (Yii::$app->user->can('Secretaria') || Yii::$app->user->can('Administrador')) {
                    //registro del kardex
                    $model->paciente_id = $id;
                    $model->estado = 'Activo';
                    $model->save();
                    // return $this->redirect(['kardex', 'id' => $model->paciente_id]);
                    $this->redirect(Yii::$app->request->referrer);
                }
                if (Yii::$app->user->can('Doctor')) {
                    //registro del kardex
                    $model->paciente_id = $id;
                    $model->estado = 'Pendiente';
                    $model->save();
                    // return $this->redirect(['kardex', 'id' => $model->paciente_id]);
                    $this->redirect(Yii::$app->request->referrer);
                }
            }
        }

        return $this->render('kardex', [
            'paciente' => $paciente,
            'kardexes' => $kardexes,
            'model' => $model,
        ]);
    }

    public function actionKardexdelete($id)
    {
        // print_r("entre a Consulta");

        $model = Kardex::findOne($id);

        $pago = Pagos::find()->where(['kardex_id' => $id])->one();
        $pagoConsulta = PagoConsulta::find()->where(['kardex_id' => $id])->one();
        if ($model['tipo'] === "Tratamiento") {
            // print_r("entre a trata");
            if ($pago) {
                Yii::$app->session->setFlash('error', "El kardex tiene pagos asignados");
                return $this->redirect(Yii::$app->request->referrer);
            }else{
            }

        } else {
            if ($pagoConsulta) {
                Yii::$app->session->setFlash('error', "El kardex tiene pagos asignados");
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        // $pagoConsulta = PagoConsulta::find()->where(['kardex_id'=>$id])->one();
        // if ($model['tipo'] === "Tratamiento") {
        //     print_r("entre a trata");

        // if(!$pago){
        //     Yii::$app->session->setFlash('error', "El kardex tiene pagos asignados");
        //     return $this->redirect(Yii::$app->request->referrer);

        // }

        // if(PFunciones::pageTotal($pago->pagodetalles,'monto_pago') < $pago->monto_total || PFunciones::pageTotal($pago->pagodetalles, 'cantidad') < $pago->cantidad_jeringas){
        //     Yii::$app->session->setFlash('error', "No se pudo actualizar el estado, no se completo el pago");
        //     return $this->redirect(Yii::$app->request->referrer);

        // }

        // } else {
        //     if(!$pagoConsulta){
        //         Yii::$app->session->setFlash('error', "El kardex tiene pagos asignados");
        //         return $this->redirect(Yii::$app->request->referrer);
        //     print_r("entre a Consulta");
        //     }
        // }
        $model->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionKardexupdate($id)
    {
        $model = Kardex::findOne($id);
        $paciente = $this->findModel($id);
        $kardexes = kardex::find()->where(['paciente_id' => $model->paciente_id])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['kardex', 'id' => $model->paciente_id]);
            $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('kardex', [
            'paciente' => $paciente,
            'kardexes' => $kardexes,
            'model' => $model,
        ]);
    }

    public function actionKardexpdf($id, $paciente_id)
    {
        $content =  $this->renderPartial('kardexpdf', [
            'paciente' => $this->findModel($paciente_id),
            'kardex' => Kardex::findOne($id),
            'kardexes' => Kardex::find()->where(['paciente_id' => $paciente_id])->all(),
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => 'letter',
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap-reporte.css',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);

        return $pdf->render();
    }

    public function actionEstado($id)
    {
        $kardex = Kardex::findOne($id);
        // $kardex_tipo = Kardex::find()->where(['tipo'=>$id])->one();
        // print_r($kardex_tipo);
        $pago = Pagos::find()->where(['kardex_id' => $id])->one();
        $pagoConsulta = PagoConsulta::find()->where(['kardex_id' => $id])->one();
        if ($kardex['tipo'] === "Tratamiento") {
            if (!$pago) {
                Yii::$app->session->setFlash('error', "El kardex no tiene nigun pago asignado");
                // return $this->redirect(['kardex', 'id' => $kardex->paciente_id]);
                return $this->redirect(Yii::$app->request->referrer);
            }

            if (PFunciones::pageTotal($pago->pagodetalles, 'monto_pago') < $pago->monto_total || PFunciones::pageTotal($pago->pagodetalles, 'cantidad') < $pago->cantidad_jeringas) {
                Yii::$app->session->setFlash('error', "No se pudo actualizar el estado, no se completo el pago");
                // return $this->redirect(['kardex', 'id' => $kardex->paciente_id]);
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            if (!$pagoConsulta) {

                Yii::$app->session->setFlash('error', "El kardex no tiene nigun pago de consulta");
                //     // return $this->redirect(['kardex', 'id' => $kardex->paciente_id]);
                return $this->redirect(Yii::$app->request->referrer);



            }
        }


        // if(!$pago){
        //     Yii::$app->session->setFlash('error', "El kardex no tiene nigun pago asignado");
        //     // return $this->redirect(['kardex', 'id' => $kardex->paciente_id]);
        //     return $this->redirect(Yii::$app->request->referrer);

        // }

        // if(PFunciones::pageTotal($pago->pagodetalles,'monto_pago') < $pago->monto_total || PFunciones::pageTotal($pago->pagodetalles, 'cantidad') < $pago->cantidad_jeringas){
        //     Yii::$app->session->setFlash('error', "No se pudo actualizar el estado, no se completo el pago");
        //     // return $this->redirect(['kardex', 'id' => $kardex->paciente_id]);
        //     return $this->redirect(Yii::$app->request->referrer);

        // }

        // //modificando el estado del kardex
        $kardex->estado = 'Activo';
        $kardex->save();

        // // return $this->redirect(['kardex', 'id' => $kardex->paciente_id]);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionReceta($id)
    {
        // print_r (Yii::$app->request->queryParams);
        $kardex = Kardex::findOne($id);
        $searchModel = new RecetaSearch();
        $searchModel->kardex_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel1 = new RecetaSearch1();
        // $searchModel1->kardex_id = $id;
        $dataProvider1 = $searchModel1->search(Yii::$app->request->queryParams,$kardex->id,$kardex->paciente_id);
        return $this->render('receta', [
            'searchModel' => $searchModel,
            'searchModel1' => $searchModel1,
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProvider1,
            'kardex' => $kardex,
            'id' => $id,
        ]);
    }

    public function actionRecetacreate($id)
    {
        $model = new Receta();
        $kardex = Kardex::findOne($id);
        $count = Receta::find()->where(['kardex_id' => $id])->count();
        if ($count > 0) {
            Yii::$app->session->setFlash('error', 'Ya fue registrada una receta');
            return $this->redirect(['receta', 'id' => $model->kardex_id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->kardex_id = $id;
            $model->save();
            return $this->redirect(['paciente/receta', 'id' => $model->kardex_id]);
        }

        return $this->render('recetaform', [
            'model' => $model,
            'kardex' => $kardex,
        ]);
    }

    public function actionRecetaupdate($id)
    {
        $model = Receta::findOne($id);
        $kardex = Kardex::findOne($model->kardex_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['paciente/receta', 'id' => $model->kardex_id]);
        }

        return $this->render('recetaform', [
            'model' => $model,
            'kardex' => $kardex,
        ]);
    }

    public function actionRecetadetalle($id)
    {
        $model = new DetalleReceta();
        $receta = Receta::findOne($id);
        $detalles = DetalleReceta::find()->where(['receta_id' => $id])->asArray()->all();
        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->receta_id = $id;
            $model->save();
            Yii::$app->session->setFlash('success', "Se registro correctamente");
            return $this->redirect(['recetadetalle', 'id' => $id]);
        }

        return $this->render('recetadetalle', [
            'detalles' => $detalles,
            'model' => $model,
            'receta' => $receta,
        ]);
    }

    public function actionRecetadetalle1($id,$kardexid)
    {
        $newkardex = Kardex::find()->where(['id' => $kardexid])->one();
        $model = new DetalleReceta();
        $receta = Receta::findOne($id);
        $detalles = DetalleReceta::find()->where(['receta_id' => $id])->asArray()->all();
        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->receta_id = $id;
            $model->save();
            Yii::$app->session->setFlash('success', "Se registro correctamente");
            return $this->redirect(['recetadetalle', 'id' => $id]);
        }

        return $this->render('recetadetalle', [
            'detalles' => $detalles,
            'model' => $model,
            'receta' => $receta,
            'newkardex' => $newkardex
        ]);
    }

    public function actionRecetadetalleupdate($id)
    {
        $model = DetalleReceta::findOne($id);
        $receta = Receta::findOne($model->receta_id);
        $detalles = DetalleReceta::find()->where(['receta_id' => $model->receta_id])->asArray()->all();
        $request = Yii::$app->request;

        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Se actualizo correctamente");
            return $this->redirect(['recetadetalle', 'id' => $model->receta_id]);
        }

        return $this->render('recetadetalle', [
            'detalles' => $detalles,
            'model' => $model,
            'receta' => $receta,
        ]);
    }

    public function actionRecetadetalledelete($id)
    {
        $model = DetalleReceta::findOne($id);
        $model->delete();
        return $this->redirect(['recetadetalle', 'id' => $model->receta_id]);
    }

    public function actionRecetadetallepdf($id)
    {
        $content =  $this->renderPartial('recetadetallepdf', [
            'receta' => Receta::findOne($id),
            'detalles' => DetalleReceta::find()->where(['receta_id' => $id])->asArray()->all(),
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => [159, 204],
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => 'css/kv-mpdf-bootstrap.css',
            // 'cssInline' => 'body {font-family: "Pepsi Ligth";}',
            'options' => ['title' => 'Krajee Report Title'],
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);

        return $pdf->render();
    }

    public function actionPagoconsulta($id)
    {
        $model = new PagoConsulta();
        $model->fecha = date('Y-m-d');
        $searchModel = new PagoConsultaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['kardex_id' => $id]);
        $kardex = Kardex::findOne($id);

        return $this->render('pagoconsulta', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kardex' => $kardex,
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionPagoconsultacreate($id)
    {
        $model = new PagoConsulta();

        if ($model->load(Yii::$app->request->post())) {
            $model->kardex_id = $id;
            $model->save();
            return $this->redirect(['paciente/pagoconsulta', 'id' => $model->kardex_id]);
        }
    }

    public function actionPagoconsultaupdate($id)
    {
        $model = PagoConsulta::findOne($id);
        $model->fecha = date('Y-m-d');
        $searchModel = new PagoConsultaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['kardex_id' => $model->kardex_id]);
        $kardex = Kardex::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->kardex_id = $id;
            $model->save();
            return $this->redirect(['paciente/pagoconsulta', 'id' => $model->kardex_id]);
        }

        return $this->render('pagoconsulta', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kardex' => $kardex,
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionPagos($id)
    {
        $searchModel = new PagosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['kardex_id' => $id]);
        $kardex = Kardex::findOne($id);

        return $this->render('pagos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kardex' => $kardex,
            'id' => $id,
        ]);
    }

    public function actionPagoscreate($id)
    {
        $model = new Pagos();
        $kardex = Kardex::findOne($id);
        $count = Pagos::find()->where(['kardex_id' => $id])->count();
        if ($count > 0) {
            Yii::$app->session->setFlash('error', 'Ya fue registrado una pago');
            return $this->redirect(['pagos', 'id' => $id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->kardex_id = $id;
            $model->save();
            return $this->redirect(['paciente/pagos', 'id' => $id]);
        }

        return $this->render('pagosform', [
            'id' => $id,
            'model' => $model,
            'kardex' => $kardex,
            'id' => $id,
        ]);
    }
    public function actionPagosupdate($id)
    {
        $model = Pagos::findOne($id);
        $kardex = Kardex::findOne($model->kardex_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['paciente/pagos', 'id' => $model->kardex_id]);
        }

        return $this->render('pagosform', [
            'model' => $model,
            'kardex' => $kardex,
            'id' => $model->kardex_id,
        ]);
    }
    public function actionPagosdetalle($id)
    {
        $model = new PagoDetalle();
        $pagos = Pagos::findOne($id);
        $detalles = PagoDetalle::find()->where(['pago_id' => $id])->all();
        $request = Yii::$app->request;
        $model->fecha = date('Y-m-d');

        if ($model->load($request->post())) {
            if ($request->post('PagoDetalle')['cantidad'] != '') {
                $tratamiento = TipoTratamiento::findOne($pagos->kardex->tratamiento_id);
                if ($tratamiento != NULL) {
                    if ($request->post('PagoDetalle')['cantidad'] > $tratamiento->cantidad || $tratamiento->cantidad <= 0) {
                        Yii::$app->session->setFlash('error', "La cantidad del tratamiento no es suficiente");
                        return $this->redirect(['pagosdetalle', 'id' => $id]);
                    }

                    if ($request->post('PagoDetalle')['cantidad'] > $pagos->cantidad_jeringas || $request->post('PagoDetalle')['monto_pago'] > $pagos->monto_total) {
                        Yii::$app->session->setFlash('error', 'Las cantidades son mayores al pago');
                        return $this->redirect(['pagosdetalle', 'id' => $id]);
                    }

                    if (PFunciones::pageTotal($detalles, 'cantidad') == $pagos->cantidad_jeringas && PFunciones::pageTotal($detalles, 'monto_pago') == $pagos->monto_total) {
                        Yii::$app->session->setFlash('error', 'Ya fue completado el pago');
                        return $this->redirect(['pagosdetalle', 'id' => $id]);
                    }

                    //modificadno la cantidad del tratamiento
                    $tratamiento->cantidad = intval($tratamiento->cantidad) - intval($request->post('PagoDetalle')['cantidad']);
                    $tratamiento->save();
                } else {
                    if ($request->post('PagoDetalle')['cantidad'] > $pagos->cantidad_jeringas || $request->post('PagoDetalle')['monto_pago'] > $pagos->monto_total) {
                        Yii::$app->session->setFlash('error', 'Las cantidades son mayores al pago');
                        return $this->redirect(['pagosdetalle', 'id' => $id]);
                    }

                    if (PFunciones::pageTotal($detalles, 'cantidad') == $pagos->cantidad_jeringas && PFunciones::pageTotal($detalles, 'monto_pago') == $pagos->monto_total) {
                        Yii::$app->session->setFlash('error', 'Ya fue completado el pago');
                        return $this->redirect(['pagosdetalle', 'id' => $id]);
                    }
                }
            }

            if (PFunciones::pageTotal($detalles, 'monto_pago') == $pagos->monto_total) {
                Yii::$app->session->setFlash('error', 'Ya fue completado el pago');
                return $this->redirect(['pagosdetalle', 'id' => $id]);
            }

            $model->pago_id = $id;
            $model->save();
            Yii::$app->session->setFlash('success', "Se registro correctamente");
            return $this->redirect(['pagosdetalle', 'id' => $model->pago_id]);
        }

        return $this->render('pagosdetalle', [
            'model' => $model,
            'pagos' => $pagos,
            'detalles' => $detalles
        ]);
    }

    public function actionViewrecetaprint($id)
    {

        $receta = Receta::findOne($id);
        $detalles = DetalleReceta::find()->where(['receta_id' => $id])->asArray()->all();

        return $this->render('viewrecetaprint', [
            'detalles' => $detalles,
            'receta' => $receta,
        ]);
    }

    public function actionCopy($id,$kardexid)
    {

        $receta = Receta::findOne($id);
        $newreceta = new Receta();
        $newreceta->attributes = $receta->attributes;
        $newreceta->kardex_id = $kardexid;
        $newreceta->fecha = date('Y-m-d');
        $newreceta->save();
        $detalles = DetalleReceta::find()->where(['receta_id' => $id])->all();
        foreach ($detalles as $detalle) {
            $newdetalle = new DetalleReceta();
            $newdetalle->attributes = $detalle->attributes;
            $newdetalle->receta_id = $newreceta->id;
            $newdetalle->save();
        }
        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Paciente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paciente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Paciente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
