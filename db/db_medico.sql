-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para db_medico
CREATE DATABASE IF NOT EXISTS `db_medico` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_medico`;

-- Volcando estructura para tabla db_medico.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_medico.auth_assignment: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('Administrador', '1', 1604427473),
	('Doctor', '2', 1612224294),
	('Secretaria', '3', 1612224303);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_medico.auth_item: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('/*', 2, NULL, NULL, NULL, 1604979575, 1604979575),
	('/paciente/*', 2, NULL, NULL, NULL, 1612221783, 1612221783),
	('/paciente/index', 2, NULL, NULL, NULL, 1612222718, 1612222718),
	('/paciente/kardex', 2, NULL, NULL, NULL, 1612222504, 1612222504),
	('/paciente/kardexupdate', 2, NULL, NULL, NULL, 1612222506, 1612222506),
	('/reserva/*', 2, NULL, NULL, NULL, 1612220836, 1612220836),
	('/tipotratamiento/*', 2, NULL, NULL, NULL, 1612220860, 1612220860),
	('Administrador', 1, NULL, NULL, NULL, 1604427443, 1604427443),
	('Doctor', 1, NULL, NULL, NULL, 1612222534, 1612222534),
	('Secretaria', 1, NULL, NULL, NULL, 1612220790, 1612220790);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_medico.auth_item_child: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('Administrador', '/*'),
	('Secretaria', '/paciente/*'),
	('Doctor', '/paciente/index'),
	('Doctor', '/paciente/kardex'),
	('Doctor', '/paciente/kardexupdate'),
	('Secretaria', '/reserva/*'),
	('Doctor', '/tipotratamiento/*'),
	('Secretaria', '/tipotratamiento/*');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_medico.auth_rule: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.kardex
CREATE TABLE IF NOT EXISTS `kardex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `consulta` enum('Si','No') NOT NULL,
  `reconsulta` enum('Si','No') NOT NULL,
  `tratamiento_id` int(11) NOT NULL,
  `detalle` varchar(1000) NOT NULL,
  `num_doc` varchar(255) NOT NULL,
  `monto` decimal(11,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` enum('Activo','Pendiente') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_kadex_paciente` (`paciente_id`),
  KEY `fk_kardex_tratamiento` (`tratamiento_id`),
  CONSTRAINT `fk_kadex_paciente` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`),
  CONSTRAINT `fk_kardex_tratamiento` FOREIGN KEY (`tratamiento_id`) REFERENCES `tipo_tratamiento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.kardex: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `kardex` DISABLE KEYS */;
INSERT INTO `kardex` (`id`, `paciente_id`, `fecha`, `consulta`, `reconsulta`, `tratamiento_id`, `detalle`, `num_doc`, `monto`, `cantidad`, `estado`) VALUES
	(1, 1, '2021-01-29', 'No', 'Si', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempus, sapien nec accumsan congue, ante velit commodo mauris, a euismod massa lacus nec urna.', 'R-110', 2000.00, 5, 'Activo'),
	(2, 1, '2021-02-01', 'Si', 'No', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor, enim id interdum viverra, urna neque facilisis arcu, eu lacinia sapien lectus id urna. Fusce sollicitudin est pellentesque blandit lobortis.', 'R-120', 2000.00, 4, 'Pendiente');
/*!40000 ALTER TABLE `kardex` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.paciente
CREATE TABLE IF NOT EXISTS `paciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `edad` int(11) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `celular` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.paciente: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` (`id`, `nombres`, `apellidos`, `fecha_nacimiento`, `edad`, `telefono`, `celular`) VALUES
	(1, 'Oscar Alberto', 'Cabrera Solíz', '1992-05-24', 29, '4370629', '60399869'),
	(2, 'Roberto', 'Rojas Camacho', '1992-05-24', 29, '4370629', '60399869'),
	(3, 'Sofia', 'Caceres Padilla', '1990-03-15', 31, '4371608', '65582210'),
	(4, 'Fabio', 'Sanchez Vergara', '1991-08-16', 30, '4440444', '76928990'),
	(5, 'Mario', 'Caceres Garcia', '1994-07-22', 27, '4846468879', '65456486515'),
	(6, 'Carmen', 'Slinas Gonzales', '1995-07-14', 26, '44649787', '789465465');
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.pcounter_save
CREATE TABLE IF NOT EXISTS `pcounter_save` (
  `save_name` varchar(10) NOT NULL,
  `save_value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`save_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.pcounter_save: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pcounter_save` DISABLE KEYS */;
INSERT INTO `pcounter_save` (`save_name`, `save_value`) VALUES
	('counter', 53),
	('day_time', 2459247),
	('max_count', 1),
	('max_time', 1604419200),
	('yesterday', 0);
/*!40000 ALTER TABLE `pcounter_save` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.pcounter_users
CREATE TABLE IF NOT EXISTS `pcounter_users` (
  `user_ip` varchar(255) NOT NULL,
  `user_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.pcounter_users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `pcounter_users` DISABLE KEYS */;
INSERT INTO `pcounter_users` (`user_ip`, `user_time`) VALUES
	('f528764d624db129b32c21fbca0cb8d6', 1612228166);
/*!40000 ALTER TABLE `pcounter_users` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.reserva
CREATE TABLE IF NOT EXISTS `reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `hora_ingreso` time DEFAULT NULL,
  `consulta` enum('Si','No') NOT NULL,
  `reconsulta` enum('Si','No') NOT NULL,
  `tratamiento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reserva_tratamiento` (`tratamiento_id`),
  KEY `fk_resrerva_paciente` (`paciente_id`),
  CONSTRAINT `fk_reserva_tratamiento` FOREIGN KEY (`tratamiento_id`) REFERENCES `tipo_tratamiento` (`id`),
  CONSTRAINT `fk_resrerva_paciente` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.reserva: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` (`id`, `paciente_id`, `fecha`, `hora`, `hora_ingreso`, `consulta`, `reconsulta`, `tratamiento_id`) VALUES
	(1, 1, '2021-01-20', '16:45:00', NULL, 'Si', 'No', 1),
	(2, 6, '2021-01-23', '11:15:00', NULL, 'Si', 'No', 3),
	(3, 4, '2021-01-25', '14:30:00', NULL, 'Si', 'No', 1),
	(4, 3, '2021-01-26', '17:15:00', NULL, 'Si', 'No', 3),
	(5, 6, '2021-01-28', '21:15:00', NULL, 'Si', 'No', 2),
	(6, 5, '2021-01-29', '10:15:00', NULL, 'Si', 'No', 3),
	(7, 1, '2021-02-01', '16:45:00', NULL, 'Si', 'No', 1);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.session
CREATE TABLE IF NOT EXISTS `session` (
  `id` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_medico.session: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` (`id`, `expire`, `data`) VALUES
	('5hr2uiesmc8tlu3hhflv0kup3v', 1608680972, _binary 0x5F5F666C6173687C613A303A7B7D),
	('8u3bd14h9tqsrkbtpe1e879fr7', 1608021025, _binary 0x5F5F666C6173687C613A303A7B7D),
	('9pjl9b0v92n2rfmhl0iqiq86mm', 1610529359, _binary 0x5F5F666C6173687C613A303A7B7D5F5F72657475726E55726C7C733A33333A22687474703A2F2F646973747269627569646F72616261636B656E642E746573742F223B5F5F69647C693A313B),
	('a1ekrmrdk318hgcv3nj4l6rivo', 1608201326, _binary 0x5F5F666C6173687C613A303A7B7D),
	('ieadue53k4p03b8rr55jrcpj2o', 1608202390, _binary 0x5F5F666C6173687C613A303A7B7D5F5F69647C693A313B),
	('ij187e1ephtkehp5o01i9ci3pj', 1608680947, _binary 0x5F5F666C6173687C613A303A7B7D5F5F72657475726E55726C7C733A33383A22687474703A2F2F646973747269627569646F72616261636B656E642E746573742F62757A6F6E223B5F5F69647C693A313B),
	('kc0mtn5edi3mvot8n8jipnk3uc', 1612264166, _binary 0x5F5F666C6173687C613A303A7B7D5F5F72657475726E55726C7C733A31393A22687474703A2F2F6D656469636F2E746573742F223B5F5F69647C693A313B),
	('kcms0qo2cb7bi95msvop2u4g6s', 1642900667, _binary 0x5F5F666C6173687C613A303A7B7D5F5F72657475726E55726C7C733A32373A22687474703A2F2F6D656469636F2E746573742F70616369656E7465223B5F5F69647C693A313B),
	('qe0a8urqc7iglvktn1c9bocu2r', 1608689681, _binary 0x5F5F666C6173687C613A303A7B7D5F5F69647C693A313B),
	('r1j24huc38spv3occgv8tc45up', 1607745063, _binary 0x5F5F666C6173687C613A303A7B7D5F5F72657475726E55726C7C733A33333A22687474703A2F2F646973747269627569646F72616261636B656E642E746573742F223B5F5F69647C693A313B);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.tipo_tratamiento
CREATE TABLE IF NOT EXISTS `tipo_tratamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.tipo_tratamiento: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_tratamiento` DISABLE KEYS */;
INSERT INTO `tipo_tratamiento` (`id`, `codigo`, `nombre`, `cantidad`) VALUES
	(1, 'c25', 'Tratamiento1', 10),
	(2, 'a15', 'Tratamiento 2', 5),
	(3, 'c14', 'Tratamiento 3', 20);
/*!40000 ALTER TABLE `tipo_tratamiento` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.trails_log
CREATE TABLE IF NOT EXISTS `trails_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `id_model` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `creation_date` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_medico.trails_log: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `trails_log` DISABLE KEYS */;
INSERT INTO `trails_log` (`id`, `description`, `action`, `model`, `id_model`, `ip`, `creation_date`, `user_id`) VALUES
	(45, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611000435, 1),
	(46, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611154028, 1),
	(47, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611240219, 1),
	(48, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1642776867, 1),
	(49, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611240924, 1),
	(50, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1642776952, 1),
	(51, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611325604, 1),
	(52, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1642863451, 1),
	(53, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1642864263, 1),
	(54, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1642864667, 1),
	(55, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1642864667, 1),
	(56, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611592189, 1),
	(57, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611681648, 1),
	(58, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611802921, 1),
	(59, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611879440, 1),
	(60, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1611927020, 1),
	(61, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1612212424, 1),
	(62, 'El usuario admin: Registro app\\models\\User[2].', 'create', 'app\\models\\User', 2, '127.0.0.1', 1612223983, 1),
	(63, 'Salio del sistema', 'logout', 'app\\models\\User', 1, '127.0.0.1', 1612224176, 1),
	(64, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 2, '127.0.0.1', 1612224180, 2),
	(65, 'Salio del sistema', 'logout', 'app\\models\\User', 2, '127.0.0.1', 1612224237, 2),
	(66, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1612224241, 1),
	(67, 'El usuario admin: Registro app\\models\\User[3].', 'create', 'app\\models\\User', 3, '127.0.0.1', 1612224269, 1),
	(68, 'Salio del sistema', 'logout', 'app\\models\\User', 1, '127.0.0.1', 1612224309, 1),
	(69, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 2, '127.0.0.1', 1612224313, 2),
	(70, 'Salio del sistema', 'logout', 'app\\models\\User', 2, '127.0.0.1', 1612225586, 2),
	(71, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 3, '127.0.0.1', 1612225591, 3),
	(72, 'Salio del sistema', 'logout', 'app\\models\\User', 3, '127.0.0.1', 1612226040, 3),
	(73, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1612226044, 1),
	(74, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612226644, 1),
	(75, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612227082, 1),
	(76, 'Salio del sistema', 'logout', 'app\\models\\User', 1, '127.0.0.1', 1612227087, 1),
	(77, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 2, '127.0.0.1', 1612227091, 2),
	(78, 'Salio del sistema', 'logout', 'app\\models\\User', 2, '127.0.0.1', 1612227095, 2),
	(79, 'Ingreso al sistema', 'login', 'mdm\\admin\\models\\User', 1, '127.0.0.1', 1612227099, 1),
	(80, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612227120, 1),
	(81, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612227347, 1),
	(82, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612227451, 1),
	(83, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612227594, 1),
	(84, 'El usuario admin: Modifico app\\models\\User[2].', 'update', 'app\\models\\User', 2, '127.0.0.1', 1612227626, 1);
/*!40000 ALTER TABLE `trails_log` ENABLE KEYS */;

-- Volcando estructura para tabla db_medico.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rol` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_medico.user: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `foto`, `rol`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'iqTP6r5C0ijmZjVkty7QwRpwwLIZxWYk', '$2y$13$YRWJWnbH3PXbMkxdyeR5husb4BLvgGwy.aEttEprXJNcNVSk2e3NC', NULL, 'admin@gmail.com', NULL, 'Administrador', 10, 1588023570, 1588023570),
	(2, 'oscar', 'WHXXYv_mQ0Iy3Qk3V80BF85WzxAUT3UF', '$2y$13$5ao7.5msiZKawirUArJE/OEdWOyQqIcgOeql3paSNrJK2fro4fNpm', NULL, 'cabrerasoliz@gmail.com', NULL, 'Doctor', 10, 1612223983, 1612223983),
	(3, 'brenda', 'FUsEaC84ruH50F3C0w7ZanGjCvFSWlj9', '$2y$13$kXEMjbDgr4xcOx7AOe8theoGjayTo4j1s35Jaemm/tdKwynSnuiLq', NULL, 'brenda@gmail.com', NULL, 'Secretaria', 10, 1612224269, 1612224269);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
